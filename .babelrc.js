module.exports = {
  plugins: ["lodash", "react-hot-loader/babel", "@babel/transform-runtime"],
  presets: [
    '@babel/preset-env',
    '@babel/preset-react',
    '@babel/preset-typescript',
  ],
}
