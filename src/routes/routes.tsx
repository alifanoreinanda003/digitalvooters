import React from 'react'
import { Route } from '../tools/types'

// import pages first
const Layout = React.lazy(() => import('../components/layout'))
const Login = React.lazy(() => import('../pages/login'))
const Beranda = React.lazy(() => import('../pages/beranda'))
const DetailData = React.lazy(() => import('../pages/detail_data'))
// add to routes
const routes: Route[] = [
  {
    name: 'Login',
    path: '/login',
    component: <Login />,
    layout: false,
    requireAuth: false
  },
  {
    name:'root',
    path:'/',
    requireAuth:true,
    component:<Layout/>,
    children:[
      {
        name: 'Beranda',
        path: 'beranda',
        component: <Beranda />,
        role: 'admin'        
      },
      {
        name: 'Detail Data',
        path: 'data',
        component: <DetailData />,   
        role: 'admin'     
      },
    ]
  } 
]

// exports
export default routes
