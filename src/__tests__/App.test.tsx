import React from 'react'
import {shallow} from 'enzyme'
import App from '../App'

test('App changes the class when hovered', () => {
  const component = shallow(<App />)
  expect(component).toMatchSnapshot()
})
