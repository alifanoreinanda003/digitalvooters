import {hot} from 'react-hot-loader/root'
import React, {Suspense} from 'react'
import {BrowserRouter, Route, Routes, Navigate} from 'react-router-dom'
import {Provider} from 'react-redux'
import {withErrorBoundary} from 'react-error-boundary'
import { parseISO } from 'date-fns'
import {routes} from './routes'
import { routesAdmin } from './routesAdmin'
import {store} from './redux/store'
import Layout from './components/layout'
import LoadingPage from './components/loadingPage'
import ErrorFallback from './components/errorFallback'

const RequireAuth: React.FC<{
  children: React.ReactNode
  redirectTo: string
}> = ({children, redirectTo}): JSX.Element => {
  const isLogin = localStorage.getItem('token_dvt')
  const isTokenInValid =  parseISO(localStorage.getItem('exp_token_dvt')) < new Date()
  
  return isLogin !== null && !isTokenInValid ? (
    (children as JSX.Element)
  ) : (
    <Navigate to={redirectTo} />
  )
}
const role = localStorage.getItem('role_dvt')

const App: React.FC = (): JSX.Element => (
  <BrowserRouter>
    <Provider store={store}>
      <Suspense
        fallback={
          <Layout isSuspense={true}>
            <LoadingPage />
          </Layout>
        }
      >
      { role === 'Admin Pusat' ?
        <Routes>
          {routesAdmin.map((route) => {
            return route.children ? (
              <Route
                key={route.name}
                path={route.path}
                element={route.component}
              >
                {route.children.map((child) => {
                  return (
                    <Route
                      key={child.name}
                      path={child.path}
                      element={
                        <RequireAuth redirectTo="/login">
                            {child.component}
                        </RequireAuth>
                      }
                    />
                  )
                })}
              </Route>
            ) : (
              <Route
                key={route.path}
                path={route.path}
                element={
                  !route.requireAuth ? (
                    route.component
                  ) : (
                    <RequireAuth redirectTo="/login">
                      route.component
                    </RequireAuth>
                  )
                }
              />
            )
          })}
        </Routes>
        : 
        <Routes>
          {routes.map((route) => {
            return route.children ? (
              <Route
                key={route.name}
                path={route.path}
                element={route.component}
              >
                {route.children.map((child) => {
                  return (
                    <Route
                      key={child.name}
                      path={child.path}
                      element={
                        <RequireAuth redirectTo="/login">
                            {child.component}
                        </RequireAuth>
                      }
                    />
                  )
                })}
              </Route>
            ) : (
              <Route
                key={route.path}
                path={route.path}
                element={
                  !route.requireAuth ? (
                    route.component
                  ) : (
                    <RequireAuth redirectTo="/login">
                      route.component
                    </RequireAuth>
                  )
                }
              />
            )
          })}
        </Routes>
        }
      </Suspense>
    </Provider>
  </BrowserRouter>
)

export default hot(withErrorBoundary(App, {FallbackComponent: ErrorFallback}))
