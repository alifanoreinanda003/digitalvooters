import {configureStore} from '@reduxjs/toolkit'
import invoiceSlice from '../slices/invoice/invoiceSlice'
import pegawaiSlice from '../slices/employee/employeeSlice'
import payrollSlice from '../slices/payroll/payrollSlice'
import masterCompSlice from '../slices/payroll/masterCompSlice'
import attendanceSlice from '../slices/attedance/attendanceSlice'
import createInvoiceSlice from '../slices/invoice/createInvoiceSlice'
import statusUserSlice  from '../slices/statusUser/statusUser'
import detailEmployeeSlice  from '../slices/employee/detailEmployeeSlice'
import detailAttendanceSlice  from '../slices/attedance/detailAttendanceSlice'
import listDataSlice  from '../slices/beranda/listData'
import listDataSuperAdminSlice  from '../slices/beranda_superAdmin/listData'
import listAdminSuperAdminSlice  from '../slices/dataAdmin_superAdmin/listData'
import summarySlice from '../slices/beranda/summary'
import listDataTimSlice from '../slices/beranda/listDataTim'
import summarySuperAdminSlice from '../slices/beranda_superAdmin/summarySuperAdmin'
import pieChartSuperAdminSlice from '../slices/beranda_superAdmin/dataPieChart'
import lineChartSuperAdminSlice from '../slices/beranda_superAdmin/dataLineChart'

export const store = configureStore({
  reducer: {
    listData : listDataSlice,
    invoices: invoiceSlice,
    payroll: payrollSlice,
    pegawai: pegawaiSlice,
    masterComp: masterCompSlice,
    attendance: attendanceSlice,
    createInvoice: createInvoiceSlice,
    statusUser : statusUserSlice,
    detailEmployee : detailEmployeeSlice,
    detailAttendance: detailAttendanceSlice,
    listDataSuperAdmin : listDataSuperAdminSlice,
    listAdminSuperAdmin : listAdminSuperAdminSlice,
    summary : summarySlice,
    listDataTim : listDataTimSlice,
    summarySuperAdmin : summarySuperAdminSlice,
    pieChartSuperAdmin : pieChartSuperAdminSlice,
    lineChartSuperAdmin : lineChartSuperAdminSlice
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
