import * as fetch from '../tools/httpRequest';
import { ApiResponse } from '../tools/types'

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

const getTimList = async (): 
Promise<ApiResponse> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
    const response = await fetch.get(`${SERVER_HOST}/tim/list?used=list`,undefined,header);
    return response as ApiResponse
} 
export { getTimList }