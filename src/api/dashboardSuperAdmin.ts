import * as fetch from '../tools/httpRequest';
import { ApiResponse, ListDataTabel, ListDataTabelParams, ListDataTim, PieChartSuperAdmin, SummarySuperAdmin } from '../tools/types';

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getListDataInput = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListDataTabel>  }>  => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/admin/v1/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}
export const getListDataTim = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListDataTim>  }>  => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/admin/v1/tim/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}

const summarySuperAdmin = async () :Promise<ApiResponse & {data: SummarySuperAdmin} > => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/summary/super-admin`,undefined,header);    
  return response as ApiResponse & {data: SummarySuperAdmin}
}
export { summarySuperAdmin }


// eslint-disable-next-line max-len
const getDataPieChart = async (month: string | unknown) :Promise<ApiResponse & {data: PieChartSuperAdmin} > => {
  const today = new Date()
  const todayMonth = today.getMonth()+1
  const params ={
    month: month === '' ? todayMonth.toString() : month
  }
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/chart/super-admin`,params,header);    
  return response as ApiResponse & {data: PieChartSuperAdmin}
}
export { getDataPieChart }

const getDataLineChart = async (month: string | unknown) :Promise<ApiResponse> => {
  
  const today = new Date()
  const todayMonth = today.getMonth()+1
  const params ={
    month: month === '' ? todayMonth.toString() : month
  }
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/grafik/super-admin`,params,header);  
    
  return response as ApiResponse
}
export { getDataLineChart }