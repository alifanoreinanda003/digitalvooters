import * as fetch from '../tools/httpRequest';
import { AddDataApiParams, AddDataParams, ApiResponse, ListDataTabel, ListDataTabelParams, ListDataTim, Summary } from '../tools/types';

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getListDataInput = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListDataTabel>  }>  => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/admin/data/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}
export const getListDataTim = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListDataTim>  }>  => {
    
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/admin/data/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}

const summary = async () :Promise<ApiResponse & {data: Summary} > => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/admin/summary`,undefined,header);    
  return response as ApiResponse & {data: Summary}
}
export { summary }

export const addData = async (params: AddDataParams): Promise<AddDataApiParams> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.post(`${SERVER_HOST}/admin/data/insert`, params,header);   
  return response as AddDataApiParams
}
