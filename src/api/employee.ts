 import * as fetch from '../tools/httpRequest';
 import { AddEmployeeApiParams, ApiResponse, ListEmployeeApiParams, ListEmployeeDataRow, PostEmployeeApiParams} from '../tools/types'

const SERVER_HOST = process.env.REACT_APP_API_HOST as string
 
const getListEmployee = async (params: ListEmployeeApiParams): Promise<ApiResponse & { 
   data: Array<ListEmployeeDataRow> }> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
   const response = await fetch.get(`${SERVER_HOST}/employed/v1/`, params,header);   
   return {
     ...response,
     data: Array.isArray(response.data) ? response.data : []
   }
 }
 export { getListEmployee }
 const deleteEmployee = async (id:string): Promise<ApiResponse> => {
   const header = {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
  const response = await fetch.remove(`${SERVER_HOST}/employed/v1/${id}`,undefined,header);     
  return response
}
export { deleteEmployee }

const getDetailEmployee = async (uuid:string): 
Promise<ApiResponse & PostEmployeeApiParams> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
    const response = await fetch.get(`${SERVER_HOST}/employed/v1/${uuid}`,undefined,header);
    return response as ApiResponse & PostEmployeeApiParams
}
export { getDetailEmployee }


const editEmployee = async (
  id:string | undefined, 
  params:AddEmployeeApiParams): 
  Promise<ApiResponse> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  const response = await fetch.edit(`${SERVER_HOST}/employed/v1/${id}`,params,header);     
  return response
}
export { editEmployee }

const addEmployee = async (params: AddEmployeeApiParams): Promise<PostEmployeeApiParams> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
  const response = await fetch.post(`${SERVER_HOST}/employed/v1/`, params,header);   
  return response as PostEmployeeApiParams
}
export {addEmployee}


