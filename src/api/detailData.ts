import * as fetch from '../tools/httpRequest';
import { ApiResponse, ConfidenceApiParams } from '../tools/types';

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

const editConfidence = async (
  id:string | undefined, 
  params:ConfidenceApiParams): 
  Promise<ApiResponse> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
    }
  const response = await fetch.edit(`${SERVER_HOST}/admin/data/insert/confidence/${id}`,params,header);     
  return response
}
export { editConfidence }
