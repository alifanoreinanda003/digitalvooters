// import * as fetch from '../tools/httpRequest';
import { 
  ApiResponse, 
  PayrollApiParams, 
  // PayrollDataRow,
  MasterCompApiParams,
  MasterCompDataRow,
  DataInput, 
} from '../tools/types'

// const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getPayroll = async (params: PayrollApiParams): Promise<ApiResponse & { 
  data: Array<DataInput> }> => {
  // const response = await fetch.get(`${SERVER_HOST}/payroll/v1/payroll`, params);
  const response = {
    success : true ,
    code : 200 ,
    data: [
      {
        no: 1,
        name : 'Johny Poni',
        nik : 3236172829384756,
        date : '3 Januari 2023',
        phone : '082330602071',
        confidence: '70%'
      },
      {
        no: 2,
        name : 'Elvi Sukaesih',
        nik : 3265731829384756,
        date : '5 Januari 2023',
        phone : '083871576645',
        confidence: '60%'
      },
      {
        no: 3,
        name : 'Jontor Abidin',
        nik : 3248197381984756,
        date : '6 Januari 2023',
        phone : '082190630447',
        confidence: '90%'
      },
      {
        no: 4,
        name : 'Uzumaki Bayu',
        nik : 3248191829363718,
        date : '9 Januari 2023',
        phone : '081219550857',
        confidence: '30%'
      },
      {
        no: 5,
        name : 'Jiraya Sensei',
        nik : 3248191829384372,
        date : '12 Januari 2023',
        phone : '085338534457',
        confidence: '50%'
      },
      {
        no: 6,
        name : 'Pesulap Merah',
        nik : 3265711829384756,
        date : '24 Januari 2023',
        phone : '081315989578',
        confidence: '80%'
      },
      {
        no: 7,
        name : 'Spongebob',
        nik : 3248191871824756,
        date : '1 Februari 2023',
        phone : '085696228417',
        confidence: '20%'
      },
    ],
    meta: {
      page: 1,
      size: 10,
      totalData: 0,
      totalDataOnPage: 0,
    },
    message:'success'
  };
  
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}

export const getMasterComp = async (params: MasterCompApiParams): Promise<ApiResponse & { 
  data: Array<MasterCompDataRow> }> => {
  // const response = await fetch.get(`${SERVER_HOST}/payroll/v1/payroll`, params);
  const response = {
    success : true ,
    code : 200 ,
    data: [
      {
        number:1,
        name:'Jaga Kucing',
        value:'Rp500.000'
      },
      {
        number:2,
        name:'Nyiram Bunga',
        value:'Rp200.000'
      },
      {
        number:3,
        name:'Cuci Mobil',
        value:'Rp100.000'
      },
    ],
    meta: {
      page: 1,
      size: 10,
      totalData: 0,
      totalDataOnPage: 0,
    },
    message:'success'
  };
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}