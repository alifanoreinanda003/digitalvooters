import * as fetch from '../tools/httpRequest'
import { PayloadAuth, ApiResponseAuth, StatusApiParams, ApiResponse, PayloadLogout, } from '../tools/types'

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

const login = async (params: PayloadAuth): Promise<ApiResponseAuth> => {
  const response =  await fetch.post(`${SERVER_HOST}/auth/login`,params);
  return response as ApiResponseAuth
}
export { login }

const getUserProfile = async (): Promise<ApiResponse> => {
  const response =  await fetch.get(`${SERVER_HOST}/auth/user/profile`);
  return response as ApiResponse
}
export { getUserProfile }

const register = async (params:PayloadAuth): Promise<ApiResponseAuth> => {
  const response =  await fetch.post(`${SERVER_HOST}/auth/v1/register`,params);
  return response as ApiResponseAuth
}
export { register }

const logout = async ():Promise<ApiResponse & PayloadLogout> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
  const response  =  await fetch.post(`${SERVER_HOST}/auth/v1/logout`,{},header);
  return response as ApiResponse & PayloadLogout
}
export { logout }

const status = async () :Promise<ApiResponse & StatusApiParams > => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/auth/user/profile`,undefined,header);    
  return response as ApiResponse & StatusApiParams
}
export { status }