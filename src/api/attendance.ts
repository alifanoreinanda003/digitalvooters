 import * as fetch from '../tools/httpRequest';
import { 
  ApiResponse,
  AttendanceApiParams, 
  EditAttendanceApiParams, 
  DetailAttendanceData 
} from '../tools/types'

 const SERVER_HOST = process.env.REACT_APP_API_HOST as string
 
 export const getAttendance = async (params: AttendanceApiParams): Promise<ApiResponse & { 
   data: Array<DetailAttendanceData> }> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
   const response = await fetch.get(`${SERVER_HOST}/attendances/v1/attendances/`, params, header);
   return {
     ...response,
     data: Array.isArray(response.data) ? response.data : []
   }
 }

 export const getDetailAttendance =async ( params:AttendanceApiParams):
 Promise<ApiResponse & {data: Array<DetailAttendanceData> }> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/attendances/v1/attendances/employees/${params.employeeUuid}`,params,header);
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
 }

 export const editAttendance = async (id:string | undefined,
   params:EditAttendanceApiParams):Promise<ApiResponse> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
   const response = await fetch.edit(`${SERVER_HOST}/attendances/v1/attendances/${id}`,params,header)
  return response
 }