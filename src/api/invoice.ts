import * as fetchAPI from '../tools/httpRequest';
import { 
  ApiResponse, 
  InvoicesApiParams, 
  InvoicesDataRow,
  InvoiceNumber,
  CreateInvoicesApiParams,
  PostInvoiceApiParams,
} from '../tools/types'

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getInvoices = async (params:InvoicesApiParams): Promise<ApiResponse & { 
  data: Array<InvoicesDataRow> }> => {
    const response = await fetchAPI.get(`${SERVER_HOST}/invoice/v1/invoice/?skip=${params.skip}&limit=${params.limit}&sortBy=invoiceDate&sortDirection=1`);
    return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}

const getInvoicesNumber = async (): Promise< ApiResponse & { 
  data: InvoiceNumber }> => {
    const response = await fetchAPI.get(`${ SERVER_HOST }/invoice/v1/invoice/invoice-number`);
    return {
    ...response,
    data: response.data as InvoiceNumber
  }
}
export { getInvoicesNumber }

const previewInvoice =  async (): Promise<BlobPart> => {
  const res =  await fetchAPI.baseFetch(`${ SERVER_HOST }/invoice/v1/invoice/prev`,'GET', undefined, undefined, { 
    'Accept': 'application/pdf',
    'Content-Type': 'application/json',
  });
  const data = await res.blob();
  return data;
  
}
export { previewInvoice }

const createInvoices = async (params: CreateInvoicesApiParams): Promise<PostInvoiceApiParams> => {
    const response =  await fetchAPI.post(`${ SERVER_HOST }/invoice/v1/invoice`,params);
    return response as PostInvoiceApiParams
}
export { createInvoices }
