import * as fetch from '../tools/httpRequest';
import { ApiResponse, ListDataTabelParams, ListDataTabelSuperAdmin, UploadFile, UploadFileApiParams } from '../tools/types';

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getListSuperAdmin = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListDataTabelSuperAdmin>  }>  => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/super-admin/data/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}
export const getFile = async () => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/dvt/v1/super-admin/download`,undefined,header)
  return response
}

export const uploadFile = async (params: UploadFile): Promise<UploadFileApiParams> => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.post(`${SERVER_HOST}/dvt/v1/super/admin/upload`, params,header);   
  return response as UploadFileApiParams
}

const deleteData = async (id:number): Promise<ApiResponse> => {
  const header = {
   Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
 }
 const response = await fetch.remove(`${SERVER_HOST}/super-admin/data/delete/${id}`,undefined,header);     
 return response
}
export { deleteData }

const updateData = async (
  id:string | number |undefined, 
  params:unknown): 
  Promise<ApiResponse> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
    }
  const response = await fetch.edit(`${SERVER_HOST}/super-admin/data/update/${id}`,params,header);     
  return response
}
export { updateData }