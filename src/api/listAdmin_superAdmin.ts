import * as fetch from '../tools/httpRequest';
import { AddAdminParams, ApiResponse, EditAdminParams, ListAdminSuperAdmin, ListDataTabelParams } from '../tools/types';

const SERVER_HOST = process.env.REACT_APP_API_HOST as string

export const getListAdminSuperAdmin = async (params:ListDataTabelParams):Promise<ApiResponse & { 
  data: Array<ListAdminSuperAdmin>  }>  => {
  const header = {
    Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
  }
  const response = await fetch.get(`${SERVER_HOST}/super-admin/admin/list`,params,header)
  return {
    ...response,
    data: Array.isArray(response.data) ? response.data : []
  }
}
export const addAdmin = async (params: AddAdminParams): Promise<AddAdminParams> => {
  const response = await fetch.post(`${SERVER_HOST}/auth/v1/admin/register`, params);   
  return response as AddAdminParams
}
const deleteAdmin = async (id:number): Promise<ApiResponse> => {
  const header = {
   Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
 }
 const response = await fetch.remove(`${SERVER_HOST}/super-admin/admin/delete/admin/${id}`,undefined,header);     
 return response
}
export { deleteAdmin }

const editAdmin = async (
  id:string | undefined, 
  params:EditAdminParams): 
  Promise<ApiResponse> => {
    const header = {
      Authorization: `Bearer ${localStorage.getItem('token_dvt')}`
    }
  const response = await fetch.edit(`${SERVER_HOST}/dvt/v1/super-admin/update/${id}`,params,header);     
  return response
}
export { editAdmin }
