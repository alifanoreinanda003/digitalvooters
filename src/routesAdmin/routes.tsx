import React from 'react'
import { Route } from '../tools/types'

// import pages first
const Layout = React.lazy(() => import('../components/layout'))
const Login = React.lazy(() => import('../pages/login'))
const Dashboard = React.lazy(() => import('../pages/dashboard_superAdmin'))
const Data = React.lazy(() => import('../pages/data_superAdmin'))
const DataAdmin = React.lazy(() => import('../pages/dataAdmin_superAdmin') )
// add to routes
const routesAdmin: Route[] = [
  {
    name: 'Login',
    path: '/login',
    component: <Login />,
    layout: false,
    requireAuth: false
  },
  {
    name:'root',
    path:'/',
    requireAuth:true,
    component:<Layout/>,
    children:[
      {
        name: 'Beranda SuperAdmin',
        path: 'beranda',
        component: <Dashboard />,
      },
      {
        name: 'Data SuperAdmin',
        path: 'data-superAdmin',
        component: <Data />,
      },
      {
        name: 'DataAdmin - SuperAdmin',
        path: 'dataAdmin-superAdmin',
        component: <DataAdmin />,
        role: 'superAdmin'        
      },
    ]
  } 
]

// exports
export default routesAdmin
