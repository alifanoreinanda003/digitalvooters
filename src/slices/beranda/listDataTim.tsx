import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as dashboard from '../../api/dashboard';
import { MetaPagination, ListDataTabelParams, ListDataTim} from '../../tools/types'

interface ListTabelState {
  data: Array<ListDataTim>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: ListTabelState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchListDataTim = createAsyncThunk(
  'listTabel/getListTabel',
  async (params:ListDataTabelParams) => {
    const response = await dashboard.getListDataTim(params)
    return response
  }
)

export const listDataTimSlice = createSlice({
  name: 'List Data Tim',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchListDataTim.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchListDataTim.fulfilled, (state, action) => {
      const {data, meta} = action.payload      
      const refactorDate: Array<ListDataTim> = data.map((row, i) => ({
        ...row,
        no: (meta.page - 1) * 10 + (i + 1),
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchListDataTim.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = listDataTimSlice.actions

export default listDataTimSlice.reducer