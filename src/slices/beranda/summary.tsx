import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { summary } from '../../api/dashboard'
import { Summary } from '../../tools/types'

const initialState: Summary = {
  code: null,
  success: false,
  message: '',
  data: {
    confidence: '',
    verification: null,
    totalData: null,
    target: null
  }
} 

const fetchSummary = createAsyncThunk(
  'summary/getSummary',
  async () => {
    const response = await summary()      
    return response.data  
  }
)
export { fetchSummary }

export const summarySlice = createSlice({
  name: 'summary',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchSummary.pending, (state) => {
      state.success = false
    })
    builder.addCase(fetchSummary.fulfilled, (state, action) => {
      const data = action.payload
      state.data = data
    })
    builder.addCase(fetchSummary.rejected, (state) => {
      state.success = false 
    })
  }
})


export const { setLoading } = summarySlice.actions
export default summarySlice.reducer