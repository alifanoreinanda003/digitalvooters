import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as masterCompApi from '../../api/payroll';
import {MasterCompApiParams, MasterCompDataRow, MetaPagination} from '../../tools/types'

interface MasterCompState {
  data: Array<MasterCompDataRow>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: MasterCompState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchMasterComp = createAsyncThunk(
  'masterComp/getMasterComp',
  async (params: MasterCompApiParams) => {
    const response = await masterCompApi.getMasterComp(params)
    return response
  }
)

export const masterCompSlice = createSlice({
  name: 'Daftar Komponen',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchMasterComp.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchMasterComp.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<MasterCompDataRow> = data.map((row, i) => ({
        ...row,
        no: ((meta.page - 1) * meta.size) + (i+1)
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchMasterComp.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = masterCompSlice.actions

export default masterCompSlice.reducer