import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as payrollApi from '../../api/payroll';
import {PayrollApiParams, MetaPagination, DataInput} from '../../tools/types'

interface PayrollState {
  data: Array<DataInput>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: PayrollState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchPayroll = createAsyncThunk(
  'payroll/getPayroll',
  async (params: PayrollApiParams) => {
    const response = await payrollApi.getPayroll(params)
    return response
  }
)

export const payrollSlice = createSlice({
  name: 'Daftar Karyawan',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchPayroll.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchPayroll.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<DataInput> = data.map((row, i) => ({
        ...row,
        no: ((meta.page - 1) * meta.size) + (i+1)
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchPayroll.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = payrollSlice.actions

export default payrollSlice.reducer