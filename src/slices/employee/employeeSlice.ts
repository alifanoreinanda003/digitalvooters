import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import * as listEmployee from '../../api/employee';
import { ListEmployeeApiParams, ListEmployeeDataRow, MetaPagination } from '../../tools/types'

interface ListEmployeeState {
  data: Array<ListEmployeeDataRow>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: ListEmployeeState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchListEmployee = createAsyncThunk(
  'listEmployee/getListEmployee',
  async (params: ListEmployeeApiParams) => {
    const response = await listEmployee.getListEmployee(params)
    return response
  }
)

export const PegawaiSlice = createSlice({
  name: 'Daftar Karyawan',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchListEmployee.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchListEmployee.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<ListEmployeeDataRow> = data.map((row, i) => ({
        ...row,
        no: ((meta.page - 1) * meta.size) + (i+1)
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchListEmployee.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = PegawaiSlice.actions

export default PegawaiSlice.reducer