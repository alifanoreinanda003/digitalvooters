import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { addEmployee } from '../../api/employee'
import {AddEmployeeApiParams } from '../../tools/types'

export interface CreateEmployeeState {
  loading:boolean,
  error:boolean,
}

const initialState: CreateEmployeeState = {
  loading:true,
  error:false,
}

const postEmployee = createAsyncThunk (
  'addEmployee/postEmployee',
 async (params:AddEmployeeApiParams) => {
  const response = await addEmployee (params)
  return response
 }
)
export {postEmployee}

export const createAddEmployeeSlice = createSlice({
  name:'addEmployee',
  initialState,
  reducers:{},
})


export default createAddEmployeeSlice