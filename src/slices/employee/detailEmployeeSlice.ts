import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import * as detailEmployee from '../../api/employee';
import { PostEmployeeApiParams } from '../../tools/types'

interface DetailEmployeeState {
  data: PostEmployeeApiParams,
  loading: boolean,
  error: boolean,  
  success: boolean
}

const initialState: DetailEmployeeState = {
  data : {
  code: null ,
  success: false,
  message: '',
  data: {
    uuid: '',
    no: null,
    name: '',
    gender: '',
    email: '',
    phoneNumber: '',
    maritalStatus: '',
    role: '',
    employedAt: '',
  }
  },
  loading: true,
  error:false,
  success: false,
}

export const getDetailEmployee = createAsyncThunk(
  'detailEmployee/getDetailEmployee',
  async (uuid:string) => {
    const response = await detailEmployee.getDetailEmployee(uuid)
    return response 
  }
)

export const employeeDetail = createSlice({
  name: 'Detail Emplyee',
  initialState,
  reducers:{
    setLoading: (state,{payload}) => ({
      ...state,
      loading:payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(getDetailEmployee.pending, (state) => {
      state.error = false
    })
    builder.addCase(getDetailEmployee.fulfilled, (state,action) => {
      const data  = action.payload
      state.data = data
      state.loading = false
    })
    builder.addCase(getDetailEmployee.rejected, (state) => {
      state.success = false
      state.loading = false
      state.error = true
    })
  }
})

export const { setLoading } = employeeDetail.actions
export default employeeDetail.reducer