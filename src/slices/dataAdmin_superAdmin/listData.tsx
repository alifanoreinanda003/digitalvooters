import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as dataAdminSuperAdmin from '../../api/listAdmin_superAdmin';
import { MetaPagination, ListDataTabelParams, ListAdminSuperAdmin } from '../../tools/types'

interface ListTabelState {
  data: Array<ListAdminSuperAdmin>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: ListTabelState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchListDataAdmin = createAsyncThunk(
  'listTabel/getListTabel',
  async (params:ListDataTabelParams) => {
    const response = await dataAdminSuperAdmin.getListAdminSuperAdmin(params)
    return response
  }
)

export const listDataSlice = createSlice({
  name: 'List Data',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchListDataAdmin.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchListDataAdmin.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<ListAdminSuperAdmin> = data.map((row, i) => ({
        ...row,
        no: (meta.page - 1) * 10 + (i + 1),
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchListDataAdmin.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = listDataSlice.actions

export default listDataSlice.reducer