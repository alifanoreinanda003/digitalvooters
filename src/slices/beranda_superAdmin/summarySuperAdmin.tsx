import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { summarySuperAdmin } from '../../api/dashboardSuperAdmin'
import { SummarySuperAdmin } from '../../tools/types'

const initialState: SummarySuperAdmin = {
  code: null,
  success: false,
  message: '',
  data: {
    confidence: '',
    admin: 0,
    verification: 0,
    totalData: 0,
  }
} 

const fetchSummarySuperAdmin = createAsyncThunk(
  'summarySuperAdmin/getSummarySuperAdmin',
  async () => {
    const response = await summarySuperAdmin()      
    return response.data  
  }
)
export { fetchSummarySuperAdmin }

export const summarySuperAdminSlice = createSlice({
  name: 'summary super admin',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchSummarySuperAdmin.pending, (state) => {
      state.success = false
    })
    builder.addCase(fetchSummarySuperAdmin.fulfilled, (state, action) => {
      const data = action.payload
      state.data = data
    })
    builder.addCase(fetchSummarySuperAdmin.rejected, (state) => {
      state.success = false 
    })
  }
})


export const { setLoading } = summarySuperAdminSlice.actions
export default summarySuperAdminSlice.reducer