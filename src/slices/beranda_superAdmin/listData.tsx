import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as dataSuperAdmin from '../../api/listData_superAdmin';
import { MetaPagination, ListDataTabelParams, ListDataTabelSuperAdmin } from '../../tools/types'

interface ListTabelState {
  data: Array<ListDataTabelSuperAdmin>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: ListTabelState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchListDataTabel = createAsyncThunk(
  'listTabel/getListTabel',
  async (params:ListDataTabelParams) => {
    const response = await dataSuperAdmin.getListSuperAdmin(params)
    return response
  }
)

export const listDataSlice = createSlice({
  name: 'List Data',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchListDataTabel.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchListDataTabel.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<ListDataTabelSuperAdmin> = data.map((row, i) => ({
        ...row,
        no: (meta.page - 1) * 10 + (i + 1),
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchListDataTabel.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = listDataSlice.actions

export default listDataSlice.reducer