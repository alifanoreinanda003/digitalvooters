import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { getDataLineChart } from '../../api/dashboardSuperAdmin'
// import { PieChartSuperAdmin } from '../../tools/types'

const initialState = {
  code: null,
  success: false,
  message: '',
  data: [
    {
      startWeek: '',
      endWeek: '',
      totalData: ''
    },
  ]
} 

const fetchLineChartSuperAdmin = createAsyncThunk(
  'lineChartSuperAdmin/getLineChartSummarySuperAdmin',
  async (params: string | unknown) => {
    const response = await getDataLineChart(params)      
    return response.data  
  }
)
export { fetchLineChartSuperAdmin }

export const lineChartSuperAdminSlice = createSlice({
  name: 'line chart super admin',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload,
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLineChartSuperAdmin.pending, (state) => {
      state.success = false
    })
    builder.addCase(fetchLineChartSuperAdmin.fulfilled, (state, action) => {
      const data = action.payload
      state.data = data
    })
    builder.addCase(fetchLineChartSuperAdmin.rejected, (state) => {
      state.success = false 
    })
  }
})


export const { setLoading } = lineChartSuperAdminSlice.actions
export default lineChartSuperAdminSlice.reducer