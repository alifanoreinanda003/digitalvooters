import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { getDataPieChart } from '../../api/dashboardSuperAdmin'
import { PieChartSuperAdmin } from '../../tools/types'

const initialState: PieChartSuperAdmin = {
  code: null,
  success: false,
  message: '',
  data: {
    valid: 0,
    invalid: 0,
    totalData: 0
  }
} 

const fetchPieChartSuperAdmin = createAsyncThunk(
  'pieChartSuperAdmin/getPieChartSummarySuperAdmin',
  async (params: string | unknown) => {
    const response = await getDataPieChart(params)      
    return response.data  
  }
)
export { fetchPieChartSuperAdmin }

export const pieChartSuperAdminSlice = createSlice({
  name: 'pie chart super admin',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchPieChartSuperAdmin.pending, (state) => {
      state.success = false
    })
    builder.addCase(fetchPieChartSuperAdmin.fulfilled, (state, action) => {
      const data = action.payload
      state.data = data
    })
    builder.addCase(fetchPieChartSuperAdmin.rejected, (state) => {
      state.success = false 
    })
  }
})


export const { setLoading } = pieChartSuperAdminSlice.actions
export default pieChartSuperAdminSlice.reducer