import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { status } from '../../api/authService'
import { StatusApiParams } from '../../tools/types'

const initialState: StatusApiParams = {
  code: null,
  success: false,
  message: '',
  data: {
    userId: null,
    name: '',
    phone: '',
    userType: ''
  }
} 

const fetchStatusUser = createAsyncThunk(
  'status/getStatus',
  async () => {
    const response = await status()      
    return response.data  
  }
)
export { fetchStatusUser }

export const statusUserSlice = createSlice({
  name: 'statusUser',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchStatusUser.pending, (state) => {
      state.success = false
    })
    builder.addCase(fetchStatusUser.fulfilled, (state, action) => {
      const data = action.payload
      state.data = data
    })
    builder.addCase(fetchStatusUser.rejected, (state) => {
      state.success = false 
    })
  }
})


export const { setLoading } = statusUserSlice.actions
export default statusUserSlice.reducer