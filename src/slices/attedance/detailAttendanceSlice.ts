import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import *  as detailAttendance   from '../../api/attendance';
import { AttendanceApiParams, DetailAttendanceData } from '../../tools/types'

export interface DetailAttendanceState {
  data: Array<DetailAttendanceData>,
  loading:boolean,
  loadmore:boolean,
  error:boolean,
  meta: AttendanceApiParams
}

 const initialState: DetailAttendanceState = {
  data: [],
  loading:false,
  loadmore:false,
  error:false,
  meta: {
    page:1,
    size:10,
    totalData:0,
    totalDataOnPage:0
  }
}

export const  fetchDetailAttendance = createAsyncThunk(
  'attendance/getDetailAttendance',
 async ( params:AttendanceApiParams) => {
  const response = await detailAttendance.getDetailAttendance(params)
  return response 
 }
)

export const detailAttendanceSlice = createSlice({
  name: 'Detail Attendance',
  initialState,
  reducers: {
    setLoading: (state, { payload }) => ({
      ...state,
      loading:payload
    }),
    setLoadMore: (state, { payload }) => ({
      ...state,
      loadmore:payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchDetailAttendance.pending,(state) => {
      state.error = false
    })
    builder.addCase(fetchDetailAttendance.fulfilled,(state,action) => {
      const {data,meta} = action.payload
      const refactorDate:Array<DetailAttendanceData> = data.map ((row,i) => ({
        ...row,
        no: ((meta.page - 1) * meta.size) + (i+1)
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchDetailAttendance.rejected,(state,action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {setLoadMore, setLoading} = detailAttendanceSlice.actions
export default detailAttendanceSlice.reducer