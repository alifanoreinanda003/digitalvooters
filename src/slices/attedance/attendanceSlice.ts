import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import * as attendance from '../../api/attendance';
import {AttendanceApiParams, DetailAttendanceData, MetaPagination} from '../../tools/types'

interface AttendanceState {
  data: Array<DetailAttendanceData>,
  loading: boolean,
  loadmore: boolean,
  error: boolean,
  meta: MetaPagination
}

const initialState: AttendanceState = {
  data: [],
  loading: false,
  loadmore: false,
  error: false,
  meta: {
    page: 1,
    size: 10,
    totalData: 0,
    totalDataOnPage: 0,
  }
}

export const fetchAttendance = createAsyncThunk(
  'attendance/getAttendance',
  async (params: AttendanceApiParams) => {
    const response = await attendance.getAttendance(params)
    return response
  }
)

export const attendanceSlice = createSlice({
  name: 'Daftar Absensi Karyawan',
  initialState,
  reducers: {
    setLoading: (state, {payload}) => ({
      ...state,
      loading: payload
    }),
    setLoadMore: (state, {payload}) => ({
      ...state,
      loadmore: payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAttendance.pending, (state) => {
      state.error = false
    })
    builder.addCase(fetchAttendance.fulfilled, (state, action) => {
      const {data, meta} = action.payload
      const refactorDate: Array<DetailAttendanceData> = data.map((row, i) => ({
        ...row,
        no: ((meta.page - 1) * meta.size) + (i+1)
      }))
      state.data = refactorDate
      state.loading = false
      state.loadmore = false
      state.meta = meta
    })
    builder.addCase(fetchAttendance.rejected, (state, action) => {
      state.data = []
      state.loading = false
      state.loadmore = false
      state.error = true
    })
  }
})

export const {
  setLoading,
  setLoadMore,
} = attendanceSlice.actions

export default attendanceSlice.reducer