import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import { createInvoices, getInvoicesNumber} from '../../api/invoice';
import { 
  CreateInvoicesApiParams, 
  InvoicesDataRow 
} from '../../tools/types'

export interface CreateInvoiceState {
  data: Array<InvoicesDataRow>,
  loading:boolean,
  error:boolean,
}

const initialState: CreateInvoiceState = {
  data: [],
  loading: false,
  error:false,
}

const postInvoice = createAsyncThunk (
  'createInvoice/postInvoice',
async (params:CreateInvoicesApiParams) => {
  const response = await createInvoices (params)
  return response
 }
)
export { postInvoice }


const InvoiceNumber = async ():Promise<string> => {
  const response = await getInvoicesNumber()
  return response.data.invoiceNumber
}
export { InvoiceNumber }

export const createInvoiceSlice = createSlice({
  name:'createInvoice',
  initialState,
  reducers: {
    setLoading: (state,{ payload }) => ({
      ...state,
      loading:payload
    })
  },
  extraReducers: (builder) => {}
})

export const { setLoading } = createInvoiceSlice.actions
export default createInvoiceSlice.reducer