import React from 'react'
import {render} from 'react-dom'
import App from './App'
import 'react-loading-skeleton/dist/skeleton.css'
import 'react-datepicker/dist/react-datepicker.css';
import './index.css'
import {register} from './serviceWorker'

const root = document.getElementById('root')

render(<App />, root)

if (process.env.NODE_ENV === 'production') {
  register() // Register Service Worker
}
