import React, { useState } from "react";
import axios from "axios";
import styles from './styles.module.css'

const DragDropFile: React.FC = ():JSX.Element => {
  const [dragActive, setDragActive] = React.useState(false);
  const [listFile, setFileList] = useState({})
  // HandleFile
  const [files, setFiles] = useState<Blob>()
  const formData = new FormData();
  if (files){
  formData.append('csv', files);
  }
  const handleUpload = async () => {    
    async function fetchData() {
      const res = await axios.post(
        'https://servers.digivoote.com/dvt/v1/super/admin/upload',
        formData,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token_dvt')}` ,
            'Content-Type': 'multipart/form-data',
          }
        }
        );
        console.log(res.data);
      }
      fetchData();
  }
  const selectedFile = Object.values(listFile)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const inputRef:any = React.useRef(null);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const fileName = selectedFile.map((item:any) => item.name).toString()
  
    function handleFile(file:File) {
      setFileList(file)
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleDrag = (e:any) => {
      e.preventDefault();
      e.stopPropagation();
      if (e.type === "dragenter" || e.type === "dragover") {
        setDragActive(true);
      } else if (e.type === "dragleave") {
        setDragActive(false);
      }
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleDrop = (e:any) => {
      e.preventDefault();
      e.stopPropagation();
      setDragActive(false);
      if (e.dataTransfer.files && e.dataTransfer.files[0]) {
        handleFile(e.dataTransfer.files);
      }
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleChange = (e:any) => {
      e.preventDefault();
      if (e.target.files && e.target.files[0]) {
        setFiles(e.target.files);
        handleFile(e.target.files);
      }
    };
    const onButtonClick = () => {
      inputRef.current.click();
    };
  return (
    <div className="space-y-3">
      <form 
        className={styles.formFileUpload} 
        onDragEnter={handleDrag} 
        onSubmit={(e) => e.preventDefault()}>
        <input ref={inputRef} type="file" className={styles.inputFileUpload} multiple={true} onChange={handleChange} />
        <label 
          htmlFor="input-file-upload" 
          className= {
            dragActive ? styles.dragActive : styles.drag
          }
        >
          <div className="grid place-items-center space-y-2">
            { selectedFile.length > 0 ? 
            <div className="grid place-items-center space-y-2" >
              <p>{fileName}</p>
              <button 
                  className={styles.uploadButton} 
                  onClick={() => handleUpload()}>
                    Upload File
                </button>
            </div>
            : 
            <div className="grid place-items-center space-y-2">
              <p>Drag and drop your file here or</p>
              <button 
                className={styles.uploadButton} 
                onClick={onButtonClick}>
                  Click Here
              </button>
            </div>
            }
          </div> 
        </label>
        { dragActive &&
          <div 
            id="drag-file-element" 
            className={styles.dragFileElement}
            onDragEnter={handleDrag} 
            onDragLeave={handleDrag} 
            onDragOver={handleDrag} 
            onDrop={handleDrop} 
          /> 
        }
      </form>
    </div>
  )
}
export { DragDropFile }