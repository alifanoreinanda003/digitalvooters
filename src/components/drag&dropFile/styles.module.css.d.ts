declare const styles: {
  readonly "drag": string;
  readonly "dragActive": string;
  readonly "dragFileElement": string;
  readonly "formFileUpload": string;
  readonly "inputFileUpload": string;
  readonly "uploadButton": string;
};
export = styles;

