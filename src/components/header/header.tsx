import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import MenuIcon from '../../assets/icons/fi-sr-menu-burger.svg'
import Floating from './floating'
import IcUser from '../../assets/images/user.png'
import icArrow from '../../assets/icons/ic-arrow.svg'
import styles from './styles.module.css'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchStatusUser } from '../../slices/statusUser/statusUser'

export interface HeaderProps {
  setNavbar: () => void
}

const Header: React.FC<HeaderProps> = ({ setNavbar }): JSX.Element => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const [ showDropdown, setDropdown ] = useState<boolean>(false)
  const { data } = useAppSelector(state => state.statusUser)
  
  const onLogout = async () => {
    try {
      setDropdown(false)
      localStorage.removeItem('token_dvt')
      localStorage.removeItem('exp_token_dvt')
      navigate('/login')
      return true
    } 
    catch (error) {
      return false
    }
  }
  useEffect(() => {
    dispatch(fetchStatusUser())
  },[])
  
  return (
  <header className={ styles.header }>
    <div onClick={ setNavbar } role="button" onKeyDown={ () => {} } tabIndex={ 0 }>
      <img alt="menu-icon" src={ MenuIcon } />
    </div>
    <div 
      className={ styles.profile }
      role="button"
          onFocus={ () => setDropdown(true) }
          onBlur={ (e) => {
            if (! e.currentTarget.contains(e.relatedTarget)) {
              setDropdown(false)
            }
          }}
          tabIndex={ 0 }
          onKeyDown={ () => {} }
    >
      <div>
        <img alt="pp" src={ IcUser }/>
      </div>
      <div className="space-y-0 font-semibold">
        <div className="text-base text-[#091D60]">{data?.name}</div>
        <div className="text-sm text-secondary-1">{data?.userType}</div>
      </div>
      <div className="flex items-center justify-center ml-4">
        <div
          className="relative cursor-default"
          role="button"
          onFocus={ () => setDropdown(true) }
          onBlur={ (e) => {
            if (! e.currentTarget.contains(e.relatedTarget)) {
              setDropdown(false)
            }
          }}
          tabIndex={ 0 }
          onKeyDown={ () => {} }>
          <img className="cursor-pointer" alt="icon-arrow" src={ icArrow } />
          <Floating show={ showDropdown } onLogout={ onLogout } />
        </div>
      </div>
    </div>
  </header>
  )
}

export { Header }
