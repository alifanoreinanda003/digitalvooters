import React from 'react';
import styles from './styles.module.css';

export interface FloatingProps {
  show: boolean;
  onLogout: () => void;
}

const Floating: React.FC<FloatingProps> = ({
  show,
  onLogout
}): JSX.Element | null => {
  if (show) {
    return (
      <div className={styles.floating}>
        <span
          onClick={onLogout}
          tabIndex={0}
          onKeyDown={() => {}}
          role="button"
          className="font-semibold">
          Keluar
        </span>
      </div>
    );
  }

  return null;
}

export default Floating;