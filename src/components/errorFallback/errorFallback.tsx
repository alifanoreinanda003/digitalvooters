import React from 'react'

export const ErrorFallback: React.FC<{error: Error}> = ({
  error,
}): JSX.Element => {
  return (
    <div role="alert" className="grid min-h-screen place-items-center">
      <div className="flex flex-col items-center space-y-8">
        <p className="text-3xl">
          <strong className="font-semibold">
            Oh no! Something went wrong:
          </strong>
        </p>
        <div className="self-start text-red-600">
          <pre>{error?.stack}</pre>
        </div>
        <button
        className='p-4 text-white rounded-md bg-primary-1'
          onClick={() => {
            window.location.href = '/'
          }}
        >
          Kembali ke Beranda
        </button>
      </div>
    </div>
  )
}
