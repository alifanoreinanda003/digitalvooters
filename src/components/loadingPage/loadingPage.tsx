import React from 'react'
import styles from './styles.module.css'
import SleepingWoman from '../../assets/images/sleeping-woman.webp'

export const LoadingPage = () => {
  return (
    <div className={styles.root}>
      <p>Mohon tunggu</p>
      <img src={SleepingWoman} alt="Sleeping Woman" />
      <div className={styles.outBar}>
        <div className={styles.bar} />
      </div>
    </div>
  )
}
