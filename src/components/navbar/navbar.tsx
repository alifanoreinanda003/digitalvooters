import React from 'react'
import {Link, useLocation} from 'react-router-dom'
import styles from './styles.module.css'
import { useScreen } from '../../tools/hook'
import MenuIcon from '../../assets/icons/menu-outline.svg'

export interface NavbarProps {
  hide: boolean
  setNavbar: () => void
}

const Navbar: React.FC<NavbarProps> = ({ hide, setNavbar }): JSX.Element => {
  const {pathname} = useLocation()
  const { isMobile } = useScreen()
  const role = localStorage.getItem('role_dvt')
  const menuAdmin = [
    {
      name: 'Beranda',
      path: '/beranda',
      icon: require('../../assets/icons/fi-rr-layout-fluid.svg')
    },
    {
      name: 'Detail Data',
      path: '/data',
      icon: require('../../assets/icons/fi-rr-copy-alt.svg')
    },
 
  ]
  const menuSuperAdmin = [
    {
      name: 'Dashboard',
      path: '/beranda',
      icon: require('../../assets/icons/fi-rr-layout-fluid.svg')
    },
    {
      name: 'Data Input',
      path: '/data-superAdmin',
      icon: require('../../assets/icons/fi-rr-copy-alt.svg')
    },
    {
      name: 'Data Admin',
      path: '/dataAdmin-superAdmin',
      icon: require('../../assets/icons/fi-rr-comment-user.svg')
    },
  ]

  return (
    <div className={`${styles.wrapper} ${isMobile ? styles.mobile : ''} ${hide ? styles.hide : ''}`}>
      <div>
        <div className={styles.title}>
          <span className="font-bold">DigiVote.</span>
         {
            isMobile && (
            <div className={styles.menuIcon} onClick={setNavbar} role="button" onKeyDown={() => {}} tabIndex={0}>
              <img alt="menu-icon" src={MenuIcon} />
            </div>
            )
          }
        </div>
        <nav className={styles.nav}>
          <div>Navigasi</div>
          <ul>
            { role !== 'Admin Pusat' ?
            menuAdmin.map((item) => {
              const selected: string = pathname === item.path ? 'text-[#091D60]' : 'text-[#D6D5D7]'
              return (
                <li key={item.name}>
                  <Link className={selected} to={item.path}>
                    <div className="flex items-start space-x-2 hover:translate-x-5 transition-transform delay-100">
                      <img src={item.icon} alt='icon-submenu' />
                      <span className=" word-break">{item.name}</span>
                    </div>
                  </Link>
                </li>
              )
            })
          : 
          menuSuperAdmin.map((item) => {
            const selected: string = pathname === item.path ? 'text-[#091D60]' : 'text-[#D6D5D7]'
            return (
              <li key={item.name}>
                <Link className={selected} to={item.path}>
                  <div className="flex items-start space-x-2 hover:translate-x-5 transition-transform delay-100">
                    <img src={item.icon} alt='icon-submenu' />
                    <span className=" word-break">{item.name}</span>
                  </div>
                </Link>
              </li>
            )
          }) 
          }
          </ul>
        </nav>
      </div>
    </div>
  )
}

export {Navbar}
