declare const styles: {
  readonly "hide": string;
  readonly "menuIcon": string;
  readonly "mobile": string;
  readonly "nav": string;
  readonly "title": string;
  readonly "wrapper": string;
};
export = styles;

