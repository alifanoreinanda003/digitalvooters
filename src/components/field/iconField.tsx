import React, { useEffect, useRef, useState } from 'react'
import { isNaN, useField } from 'formik'
import styles from './styles.module.css'

export interface IconFieldProps {
  name: string
  className?: string
  placeholder?: string
}

const defaultIconFieldProps = {
  className: '',
  placeholder: '',
}

const IconField: React.FC<IconFieldProps> = (props): JSX.Element => {
  const { name, className, placeholder } = props
  const [ field ] = useField(name)
  const wrapperClass = ['relative', className].filter(Boolean).join(' ')
  const refIcon = useRef<HTMLDivElement>(null)
  const [pl, setPl] = useState<number>()

  useEffect(() => {
    setPl(Number(refIcon.current?.offsetWidth))
  }, [refIcon])

  function FormatCurrency( input: number | null ) {
    const number:number = input === null ? 0 : input
    return new Intl.NumberFormat('id-ID',{
      minimumFractionDigits: 0,
      maximumFractionDigits: 2
    }).format(number);
  }
  
  return (
    <div className={ wrapperClass }>
      <div ref={ refIcon } className={ styles.iconField }>RP</div>
      <input
        name={ name }
        className="w-full"
        placeholder={ placeholder }
        autoComplete='off'
        value={field.value}
        onChange={(e) => {
          const inputValue:number = Number(e.target.value.replace(/\./ig, '',))          
          if(isNaN(inputValue) && field.value === null){
            field.onChange({
              target:{
                value:0,
                name:e.target.name
              }
            })
          }
          if(!isNaN(inputValue)){
          const formatResult = FormatCurrency(inputValue)          
            field.onChange({
              target:{
                value:formatResult === '0' ? '' : formatResult,
                name:e.target.name
              }
            }) 
          }
        }}
        maxLength={ 12 }
        type='text'
        style={{
          paddingLeft: `${pl}px`
        }}
      />
    </div>
  )
}

IconField.defaultProps = defaultIconFieldProps

export default IconField