import React, { useRef } from 'react'
import { useField } from 'formik'
import { id } from 'date-fns/locale'
import DatePicker from 'react-datepicker'
import styles from './styles.module.css'
import IcCalendar from '../../assets/icons/ic-calendar.svg'

export interface CustomInputProps {
  className?: string
  placeholder?: string
  onClick?: () => void
  value?: string

};

const defaultCustomInputProps = {
  className: '',
  placeholder: '',
  onClick: () => { },
  value: '',
}

export const CustomInput = React.forwardRef<HTMLDivElement, CustomInputProps>((props, ref) => {
  const { className, placeholder, onClick, value,} = props
  const wrapperClass = ['relative', className].filter(Boolean).join(' ')
  const refIcon = useRef<HTMLDivElement>(null)

  return (
    <div className={wrapperClass}>
      <input
        className="w-full"
        onClick={onClick}
        placeholder={placeholder}
        defaultValue={value}
        readOnly
        style={{
          paddingRight: `${refIcon.current?.offsetWidth}px`
        }}
      />
      <div ref={refIcon} className={styles.iconDate}>
        <img src={IcCalendar} alt="icon calendar" />
      </div>
    </div>
  )
})

CustomInput.defaultProps = defaultCustomInputProps

export interface DateFieldProps {
  name: string
  className?: string
  placeholder?: string
  max?: Date | null |undefined 
  min?: Date | null |undefined 
}

const defaultDateFieldProps = {
  className: '',
  placeholder: '',
  max: undefined,
  min: undefined,
}

const DateField: React.FC<DateFieldProps> = (props): JSX.Element => {
  const { name, className, placeholder, max, min,} = props
  const [ field ] = useField(name)

  
  return (
    <DatePicker
      className={ className }
      dateFormat="dd/MM/yyy"
      locale={ id }
      maxDate={ max }
      minDate={ min }
      placeholderText={ placeholder }
      selected={ field.value }
      onChange={ (data) => field.onChange({ target: { value: data, name }}) }
      customInput={ <CustomInput /> }
      
    />
  )
}

DateField.defaultProps = defaultDateFieldProps

export default DateField