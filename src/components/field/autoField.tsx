import React, { useRef, useState, useEffect } from 'react'
import { useField } from 'formik'
import styles from './styles.module.css'

export interface AutoFieldProps {
  name: string
  className?: string
  placeholder?: string
  value: string
  getInvoiceNumber: (value: boolean) => void
  setIsCheck: (value:boolean) => void
}

const defaultAutoFieldProps = {
  className: '',
  placeholder: ''
}

const AutoField: React.FC<AutoFieldProps> = (props): JSX.Element => {
  const { name, className, placeholder, getInvoiceNumber, value, setIsCheck} = props
  const [ field ] = useField(name)
  const [ check, setCheck ] = useState(true);
  const wrapperClass = [ 'relative', className ].filter(Boolean).join(' ')
  const refAuto = useRef<HTMLDivElement>(null)
  
  useEffect(() => {
    getInvoiceNumber(check)
    setIsCheck(check)
  },[check])
  
  useEffect(() => {
    field.onChange({ target: { name, value }})
  },[value])

  return (
    <div className={ wrapperClass }>
      <input
        name={ name }
        type="text"
        className="w-full"
        readOnly={ check }
        placeholder={ check ? 'Masukkan Nomor Invoice' : placeholder}
        value={ field.value }
        onChange={ e => {
          field.onChange(e)
        }}
        style={{
          paddingRight: `${Number(refAuto.current?.offsetWidth) + 8}px`
        }}
      />
      <div ref={ refAuto } className={ styles.checkboxAuto }>
        <input
          type="checkbox"
          checked={ check }
          id="auto"
          autoComplete='off'
          name="vehicle1"
          onChange={() => {
            setCheck(!check)
          }}
        />
        <label className="ml-1 font-semibold text-sm" htmlFor="auto">Otomatis</label>
      </div>
    </div>
  )
}

AutoField.defaultProps = defaultAutoFieldProps

export default AutoField