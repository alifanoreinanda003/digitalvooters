import React from 'react'
import { useField } from 'formik'

export interface ProjectNameProps {
  name: string
  className?: string
  placeholder?: string
  type?: string
}

const defaultProjectNameProps = {
  className: '',
  placeholder: '',
  type: 'text'
}

const ProjectName: React.FC<ProjectNameProps> = (props): JSX.Element => {
  const { name, className, placeholder, type } = props
  const [ field ] = useField(name)
  const wrapperClass = ['relative', className].filter(Boolean).join(' ')

  return (
    <div className={wrapperClass}>
      <input
        name={name}
        className="w-full"
        placeholder={placeholder}
        autoComplete="off"
        value={field.value}
        onChange={field.onChange}
        maxLength={70}
        type={type}
      />
    </div>
  )
}

ProjectName.defaultProps = defaultProjectNameProps

export default ProjectName