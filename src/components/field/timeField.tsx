import React, { useRef } from 'react'
import { useField } from 'formik'
import DatePicker from 'react-datepicker'
import styles from './styles.module.css'
import IcCalendar from '../../assets/icons/ic-calendar.svg'

export interface CustomInputProps {
  className?: string
  placeholder?: string
  onClick?: () => void
  value?: string
};

const defaultCustomInputProps = {
  className: '',
  placeholder: '',
  onClick: () => { },
  value: ''
}

export const CustomInput = React.forwardRef<HTMLDivElement, CustomInputProps>((props, ref) => {
  const { className, placeholder, onClick, value } = props
  const wrapperClass = ['relative', className].filter(Boolean).join(' ')
  const refIcon = useRef<HTMLDivElement>(null)

  return (
    <div className={wrapperClass}>
      <input
        className="w-full "
        onClick={onClick}
        placeholder={placeholder}
        defaultValue={value}
        readOnly
        style={{
          paddingRight: `${refIcon.current?.offsetWidth}px`
        }}
      />
      <div ref={refIcon} className={styles.iconDate}>
        <img src={IcCalendar} alt="icon calendar" />
      </div>
    </div>
  )
})

CustomInput.defaultProps = defaultCustomInputProps

export interface TimeFieldProps {
  name: string
  className?: string
  placeholder?: string
}

const defaultTimeFieldProps = {
  className: '',
  placeholder: ''
}

const TimeField: React.FC<TimeFieldProps> = (props): JSX.Element => {
  const { name, className, placeholder } = props
  const [field] = useField(name)
  return (
    <DatePicker
      className={className}
      showTimeSelect
      showTimeSelectOnly
      timeIntervals={15}
      timeCaption="Time"
      dateFormat="h:mm aa"
      placeholderText={placeholder}
      selected={field.value}
      onChange={(data) => field.onChange({ target: { value: data, name }})}
      customInput={<CustomInput />}
      {...props}
    />
  )
}

TimeField.defaultProps = defaultTimeFieldProps

export default TimeField