import React from 'react'
import { useField } from 'formik'

export interface NumberFieldProps {
  name : string
  className? : string
  placeholder? : string
  type?: string
}

const defaultNumberFieldProps = {
  className: '',
  placeholder: '',
  type: 'text'
}


const NumberField: React.FC<NumberFieldProps> = (props) : JSX.Element => {
  const { name,className, placeholder,type} = props
  const [ field ] = useField(name)
  const wrapperClass = ['relative', className].filter(Boolean).join('')
  
  return (
    <div className={wrapperClass}>
      <input
        name={name}
        className= "w-full"
        placeholder={placeholder}
        autoComplete="off"
        value={field.value.replace(/[^0-9]/g, '')}
        onChange={field.onChange}
        type={type}
      />
    </div>
  )
}

NumberField.defaultProps = defaultNumberFieldProps

export default NumberField