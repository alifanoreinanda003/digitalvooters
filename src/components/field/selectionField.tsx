import React from 'react'
import { useField } from 'formik'
import Select, { StylesConfig } from 'react-select'

type MyOptionType = {
  label: string | undefined;
  value: object | number | string| undefined;
};
export interface Props {
  name: string,
  placeholder: string,
  selectOptions: Array<{ value: number | string, label: string }>,
  defaultDetail?: string | undefined
}

const defaultProps = {
  defaultDetail: undefined
}

const SelectionField: React.FC<Props> = (props): JSX.Element => {
  const { name, placeholder, selectOptions, defaultDetail } = props
  const [ field ] = useField(name)
  
  type IsMulti = false;
  const selectStyle: StylesConfig<MyOptionType,IsMulti> = {
    control: (base) =>  ({
      ...base,
      background:'rgba(9,29,96,0.1)',
      height:'3rem',
      fontSize:'14px',
      borderRadius:'10px',
      fontWeight:'500',
      outline:'none',
      border:'none',
      paddingBottom:'0.6rem',
      colors:'black',
    }),
    placeholder: (base) => ({
      ...base,
      color:'#091D60',
      marginBottom:'0.5rem'
    }),
    option: (base, state) => ({
      ...base,      
      color: "black",
      fontWeight:'500',
      fontSize:'14px',
      overflow:'hidden',
    }),
    dropdownIndicator: (base) => ({
      ...base,
      marginBottom:'0.5rem',
    }),
    valueContainer: (provided) => {
      return {
        ...provided,
        minHeight: "auto",
        flexWrap: "nowrap",
        overflowX: "auto",
        padding: "0 .5rem",
        color:'white',
      };
    },
    input:(base) => ({
      ...base,
      color:"white"
    }),
    singleValue:(base) => ({
      ...base,
      color:'black',
      marginBottom:'0.5rem'
    })
  };
  return(
    <div>
        <Select 
            id="company"
            options={ selectOptions }
            styles={ selectStyle }
            placeholder={ placeholder }
            defaultValue={{ label:defaultDetail, value:defaultDetail }}
            components={{ IndicatorSeparator:() => null }}
            onChange={(val) => field.onChange({
              target: { value: val?.value , name },              
            })
          }
        />
    </div>
  )
}
SelectionField.defaultProps = defaultProps
export default SelectionField