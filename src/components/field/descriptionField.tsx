import React from 'react'
import { useField } from 'formik'

export interface DescriptionProps {
  name: string
  className?: string
  placeholder?: string
}

const defaultDescriptionProps = {
  className: '',
  placeholder: '',
}

const Description: React.FC<DescriptionProps> = (props): JSX.Element => {
  const { name, className, placeholder } = props
  const [ field ] = useField(name)
  const wrapperClass = ['relative', className].filter(Boolean).join(' ')
  return (
    <div className={wrapperClass}>
      <textarea
        name={name}
        className="w-full text-sm font-medium p-3 rounded-10 bg-white bg-opacity-20"
        placeholder={placeholder}
        autoComplete="off"
        value={field.value}
        onChange={field.onChange}
        maxLength={120}
      />
    </div>
  )
}

Description.defaultProps = defaultDescriptionProps

export default Description