import React, { useState } from 'react'
import {Outlet, useLocation, Navigate} from 'react-router-dom'
import useScreen from '../../tools/hook/screen'
import Header from '../header'
import Navbar from '../navbar'
import styles from './styles.module.css'

type Props = {
  isSuspense?: boolean
}

const Layout: React.FC<Props> = ({isSuspense, children}): JSX.Element => {
  const {isMobile} = useScreen()
  const [navbar, setNavbar] = useState<boolean>(isMobile)
  const location = useLocation()
  const beforeAuthRoutes = ['/login', '/registration']

  if (location.pathname === '/') {
    return <Navigate to="/beranda" />
  }

  if (beforeAuthRoutes.includes(location.pathname)) {
    return (
      <main
        className={[styles.main, isSuspense ? 'min-h-screen' : ''].join(' ')}
      >
        {children}
      </main>
    )
  }

  return (
    <div className={styles.container}>
      <Navbar setNavbar={() => setNavbar(!navbar)} hide={navbar} />
      <div className="flex flex-col flex-grow w-full min-h-screen">
        <Header setNavbar={() => setNavbar(!navbar)} />
        <main
          className={[styles.main, isSuspense ? 'min-h-screen' : ''].join(' ')}
        >
          {isSuspense ? children : <Outlet />}
        </main>
      </div>
    </div>
  )
}

Layout.defaultProps = {
  isSuspense: false,
}

export {Layout}
