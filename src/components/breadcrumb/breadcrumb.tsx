import React from 'react'
import { Link } from 'react-router-dom'
import styles from './styles.module.css'
import icHome from '../../assets/icons/ic-home.svg'

interface DataBreadcrumb {
  to: string,
  name: string
}

interface PropsBreadcrumb {
  data?: Array<DataBreadcrumb>
}

const defaultProps = {
  data: []
}

const CardBreadcrumb: React.FC<PropsBreadcrumb> = ({data}): JSX.Element => {
  const section: DataBreadcrumb | null = data ? data[0] : null
  if (data) {
    data.shift()
  }
  
  return (
    <div className={styles.card}>
      {data && data.length
        ? <CardBreadcrumb data={data} />
        : <div className={styles.card}>
          <Link to="/"><img alt="home" src={icHome} /></Link>
        </div>
      }
      {section && section.to &&
        <Link to={section.to}><span>{section.name}</span></Link>
      }
      {section && !section.to &&
        <span>{section.name}</span>
      }
    </div>
  )
}

CardBreadcrumb.defaultProps = defaultProps

const Breadcrumb: React.FC<PropsBreadcrumb> = ({data}): JSX.Element => {
  const rev = data ? data.reverse() : []
  return (
    <div>
      <CardBreadcrumb data={rev} />
    </div>
  )
}

Breadcrumb.defaultProps = defaultProps

export {Breadcrumb}
