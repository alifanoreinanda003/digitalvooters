import React from 'react';
import { SkeletonTheme } from 'react-loading-skeleton'

interface ThemeProps {
  children?: React.ReactNode
}

const defaultThemeProps = {
  children: ''
}

const Theme: React.FC<ThemeProps> = ({children}): JSX.Element => {
  return (
    <SkeletonTheme baseColor="#5C6673" highlightColor="#B6B6B6">
      {children}
    </SkeletonTheme>
  )
}

Theme.defaultProps = defaultThemeProps

export default Theme