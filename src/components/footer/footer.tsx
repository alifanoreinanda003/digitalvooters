import React from 'react'
import styles from './styles.module.css'

const Footer: React.FC = (): JSX.Element => (
  <footer className={styles.footer}>
    <h4 className={styles.title}>Skyrain Studio</h4>
  </footer>
)

export {Footer}
