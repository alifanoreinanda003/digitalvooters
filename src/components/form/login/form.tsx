import React, {useState} from 'react'
import {Formik, Form, Field } from 'formik'
import {PayloadAuth} from '../../../tools/types'
import SpinnerButton from '../../spinnerButton'
import  icEyeOpen from '../../../assets/icons/fi-sr-eye.svg'
import  icEyeClose from '../../../assets/icons/fi-sr-eye-crossed.svg'
import styles from './styles.module.css'

type Props = {
  isLoading: boolean
  handleSubmit: (params: PayloadAuth) => void
  error : undefined
}
const initialValues = {
  username: '',
  password: '',
}

const FormComponent: React.FC<Props> = ({
  handleSubmit,
  isLoading,
  error
}): JSX.Element => { 
   
  const [typePsw, setTypePsw] = useState<'password' | 'text'>('password')
  const toggleType = (type: 'password' | 'text') =>
    type === 'password' ? 'text' : 'password'

  const validate = (values:PayloadAuth) => {    
    const errors = {};     
    if(values.username && values.username.length < 3){
      Object.assign(errors,{
        username: 'Username minimal 3 karakter'
      })
    }
    if(values.password && values.password.length < 8){
      Object.assign(errors,{
        password: 'Password minimal 8 karakter'
      })
    }
    return errors
  }

  return (
    <Formik
    initialValues={ initialValues }
    validate={ validate }
    >
      {({values}) => {
        
        let disabled = true
        if (
          values.username && values.password 
        )
          disabled = false
        if ( values.username && values.password)
          disabled = false

        return (
          <Form>
            <div className={styles.inputGruop}>
              <div className="flex-grow">
                <label htmlFor="username">Nama Pengguna</label>
                <Field
                  type="text"
                  name="username"
                  id="username"
                  autoComplete="off"
                />
              </div>
            </div>
            <div className={styles.inputGruop}>
              <div>
                <label htmlFor="password">Kata Sandi</label>
                <Field
                  type={typePsw}
                  name="password"
                  id="password"
                  autoComplete="off"
                />
              </div>
              <div
                className="self-end"
                onClick={() => setTypePsw(toggleType(typePsw))}
                tabIndex={0}
                onKeyPress={() => {}}
                role="button"
                >
                <img
                  className="cursor-pointer"
                  alt="icon eye open"
                  src={typePsw === 'password' ? icEyeClose : icEyeOpen}
                  />
              </div>
            </div>
            {
            error !== undefined ? <p className='text-sm text-red text-center'>{error?.message}</p> : ''
            }
            <button
              type="button"
              className={styles.submit}
              disabled={isLoading === true || disabled}
              onClick={() => handleSubmit(values)}
            >
              {isLoading === true ? (
                <div className="flex justify-center items-center">
                  <SpinnerButton />
                </div>
              ) : (
                <div>Masuk</div>
              )}
            </button>
          </Form>
        )
      }}
    </Formik>
  )
}

// FormComponent.defaultProps = defaultProps

export default FormComponent
