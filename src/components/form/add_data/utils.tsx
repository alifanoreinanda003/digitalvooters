import { AddDataParams } from "../../../tools/types";

export const validate = (values:AddDataParams) => {    
  const errors = {};     
  
  if(!values.timName ){
    Object.assign(errors,{
      timName: 'Nama tim wajib diisi'
    })
  }
  if(!values.name){
    Object.assign(errors,{
      name: 'Nama wajib diisi'
    })
  }
  if(values.name && values.name.length < 3){
    Object.assign(errors,{
      name: 'Nama minimal 3 karakter'
    })
  }
  if(!values.nik){
    Object.assign(errors,{
      nik: 'NIK Wajib Diisi'
    })
  }
  if(values.nik && values.nik.length < 16 ){
    Object.assign(errors,{
      nik: 'NIK wajib 16 karakter'
    })
  }
  if(values.phone && values.phone.length < 12){
    Object.assign(errors,{
      phone: 'Nomor Telefon wajib 12 karakter'
    })
  }
  if(!values.phone){
    Object.assign(errors,{
      phone: 'Nomor Telefon wajib diisi'
    })
  }
  return errors
}