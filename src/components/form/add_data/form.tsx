import React, { useState } from 'react'
import { Formik, Form, Field } from 'formik'
import { addData } from '../../../api/dashboard';
import InformationAlert from '../../popUp/informationAlert';
import style from './style.module.css'
import { AddDataParams } from '../../../tools/types';
import { fetchListDataTabel } from '../../../slices/beranda/listData';
import { validate } from './utils';

const FormComponent: React.FC = (): JSX.Element => {
  const initialValues = {
    timName:'',
    nik:'',
    name:'',
    phone:'',
  };
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  }
  const [openModal, setOpenModal] = useState(false)
  const [message, setMessage] = useState('')
  const [isSuccess, setIsSuccess] = useState(false)
  const handleSubmit = async (formValue:AddDataParams) => {
    try {
      await addData(formValue)
      fetchListDataTabel(tabelParams)
      setIsSuccess(true)
      setOpenModal(true)
      setMessage('Data Berhasil di Input')
    } catch (error) {      
      setIsSuccess(false)
      setOpenModal(true)
      setMessage(error.message)
    }
  }
  return (
  <div>
    <Formik
      initialValues={ initialValues }
      validate={validate}
      onSubmit={(value) => { 
        handleSubmit({...value,timName:value.timName.toLowerCase()})
      }}
    >
    {({values, errors, touched}) => {
      const disabled = Object.values(errors).toString() !== ''
      return(
      <Form>
        <div className={ style.form }>
          <div className={ style.groupField }>
            <label htmlFor="timName">Tim</label>
            <Field  name="timName" id="tim" type="text" autoComplete="off" placeholder="Masukan Nama Tim" />
            <span />
          </div>
          {errors.timName && touched.timName ? (
            <div className="italic text-sm text-red">
              {errors.timName}
            </div>
          ) : null}
          <div className={ style.groupField }>
            <label htmlFor="nik">NIK</label>
            <Field name="nik" placeholder="Masukan NIK" />
            <span />
          </div>
          {errors.nik && touched.nik ? (
            <div className="italic text-sm text-red">
              {errors.nik}
            </div>
          ) : null}
          <div className={ style.groupField }>
            <label htmlFor="nama">Nama</label>
            <Field name="name" id="nama" type="text" autoComplete="off" placeholder="Masukan Nama" />
            <span />
          </div>
          {errors.name && touched.name ? (
            <div className="italic text-sm text-red">
              {errors.name}
            </div>
          ) : null}
          <div className={ style.groupField }>
            <label htmlFor="phone">Telepon</label>
            <Field name="phone" placeholder="Masukan Telepone" />
            <span />
          </div>
          {errors.phone && touched.phone ? (
            <div className="italic text-sm text-red">
              {errors.phone}
            </div>
          ) : null}
        </div>
        <div className="flex justify-between items-center space-x-2 pt-4">
          <button type="reset" className={ style.reset } onClick={() => {}}>Reset</button>
          <button 
          type="submit" 
          className={ style.submit } 
          disabled={disabled}
          >
          Submit
          </button>
        </div>
      </Form>
    )}}
    </Formik>
    <InformationAlert
      isOpen={openModal}
      message={message}
      isSuccess={isSuccess}
      setOpenModal={setOpenModal}
    />
    </div>
  )
}

export default FormComponent