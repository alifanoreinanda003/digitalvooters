import React, { useState } from 'react'
import { Formik, Form, Field } from 'formik'
import { useDispatch } from 'react-redux'
import style from './style.module.css'
import { EditAdminParams } from '../../../tools/types'
import { editAdmin } from '../../../api/listAdmin_superAdmin'
import { fetchListDataAdmin } from '../../../slices/dataAdmin_superAdmin/listData'
import { AppDispatch } from '../../../redux/store'

interface Props {
  setModalOpen: (val:boolean) => void
  initialData: object
}

const FormComponent: React.FC<Props> = (props): JSX.Element => {
  const { initialData, setModalOpen } = props  
  const dispatch = useDispatch<AppDispatch>();
  
  const initialValues = {
    name: initialData.name,
    phone: initialData.noHp,
    username: initialData.name,
    password:'',
    kodeDesa: initialData.kodeDesa || '',
    usersCategory:'admin',
  };
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  }
  const [errorRes, setErrosRes] = useState('')
  const handleSubmit = async (formValue:EditAdminParams) => {
    try {
      await editAdmin(initialData.id,formValue)
      dispatch(fetchListDataAdmin(tabelParams))
      setModalOpen(false)
    } catch (error) {
      setErrosRes('Terjadi Kesalahan Teknis')
    }
  }
  return (
  <div>
    <Formik
      initialValues={ initialValues }
      onSubmit={(value) => { 
        handleSubmit(value)
      }}
    >
    {({values}) => {                  
      const disabled = Boolean(
        values.name && 
        values.username && 
        values.phone &&
        values.kodeDesa
        )
      return(
      <Form>
        <div className={ style.form }>
          <div className={ style.groupField }>
            <label htmlFor="nama">Nama</label>
            <Field  name="name" id="nama" type="text" autoComplete="off" placeholder="Masukan Nama" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="phone">Telepon</label>
            <Field  name="phone" id="phone" type="phone" autoComplete="off" placeholder="Masukan Telepone" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="username">UserName</label>
            <Field  name="username" id="username" type="text" autoComplete="off" placeholder="Masukan Username " />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="password">Password</label>
            <Field  name="password" id="password" type="text" autoComplete="off" placeholder="************" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="kodeDesa">Kode Wilayah</label>
            <Field  name="kodeDesa" id="kodeDesa" type="text" autoComplete="off" placeholder="Masukan Kode Wilayah" />
            <span />
          </div>
        </div>
        {errorRes ? <p className="flex justify-center pt-3 text-sm text-red">Terjadi Kesalahan Teknis</p> : ''}
        <div className="flex justify-between items-center space-x-2 pt-4">
          <button 
          type="submit" 
          className={ style.submit } 
          disabled={!disabled}
          >
          Submit
          </button>
        </div>
      </Form>
    )}}
    </Formik>
    </div>
  )
}

export default FormComponent