declare const styles: {
  readonly "form": string;
  readonly "groupField": string;
  readonly "reset": string;
  readonly "submit": string;
  readonly "textArea": string;
};
export = styles;

