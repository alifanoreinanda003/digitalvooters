import React, {  useState } from 'react'
import { Formik, Form, FormikErrors } from 'formik'
import { CreateInvoicesApiParams } from '../../../tools/types'
import DateField from '../../field/dateField'
import SelectionField from '../../field/selectionField'
import AutoField from '../../field/autoField'
import ProjectName from '../../field/projectField'
import IconField from '../../field/iconField'
import Description from '../../field/descriptionField'
import styles from './styles.module.css'

export interface FormCreate {
  invoiceNumber: string
  projectName:string
  projectDecsription:string
  invoiceRelease: Date | null | undefined 
  receiver: Receiver
  invoiceDueDate: Date | null | undefined 
  ammount: number | '' 
} 
export interface Receiver {
    id: string, 
    companyName: string, 
    address:string, 
    baankAccount: string
}
export interface Props {
  createInvoice: (arg: CreateInvoicesApiParams) => void
  invoiceNumber: () => void
  updatePreview: (arg: FormCreate) => void
  value: string
  isLoading: unknown | string
}
export const initialValues: FormCreate = {
  invoiceNumber: '',
  projectName:'',
  projectDecsription:'',
  invoiceRelease: undefined,
  receiver: {
    id: '', 
    companyName: '', 
    address:'', 
    baankAccount: ''
  },
  invoiceDueDate: undefined,
  ammount: ''
};
const FormComponent: React.FC<Props> = (props): JSX.Element => {
  const [ isCheck,setIsCheck ] = useState(false)
  const selectOptions = [
    { 
      value:{
        'id':'1',
        'companyName':'Name Company',
        'address' : 'Jl.mangga',
        'bankAccount' : 'BRI'
      }, 
      label: 'Name Company' 
    },
    { 
      value:{
        'id':'2',
        'companyName':'Skyrain',
        'address' : 'Jl.rambutan',
        'bankAccount' : 'BCA'
      }, 
      label: 'Skyrain' 
    },
  ]

  const validate = (values: FormCreate) => {    
    props.updatePreview(values)
    const error = {}; 
    if (!values.invoiceNumber) {
      Object.assign(error,{
        invoiceNumber : 'This field is Required'
      })
    }
    if (!values.projectName) {
      Object.assign(error ,{
        projectName : 'This field is Required'
      })
    }
    if (!values.projectDecsription) {
      Object.assign(error,{
        projectDecsription : 'This field is Required'
      })
    }
    if (!values.invoiceRelease) {
      Object.assign(error,{
        invoiceRelease : 'This field is Required'
      })
    }
    if (!values.receiver) {
      Object.assign(error,{
        receiver : 'This field is Required'
      })
    }
    if (!values.invoiceDueDate) {
      Object.assign(error,{
        invoiceDueDate : 'This field is Required'
      })
    }
    if (!values.ammount) {
      Object.assign(error,{
        ammount : 'This field is Required'
      })
    }    
    return error
  }

  const handleDisable = (values:FormCreate , errors:FormikErrors<FormCreate>) => {
    let result = false
    if ( 
        Object.values(errors).length || 
        props.isLoading === true || 
        !values
      ){
        result = true
      } 
      return result
  }
  return (
    <Formik
      initialValues={ initialValues }
      validate={ validate } 
      onSubmit={(value) => {
        const replaceDot = value.ammount?.toString().split('.').join('')
        const formValue = {
          ...value,
          ammount: Number(replaceDot)
        }
        props.createInvoice(formValue);
      }} 
    >
    {({ errors,touched,values,resetForm,setFieldValue}) => {         
      return(
      <Form>
        <div className={styles.form}>
          <div className={styles.groupField}>
            <label htmlFor="invoiceNumber">Nomor Invoice</label>
            <AutoField  
              name="invoiceNumber" 
              value={props.value} 
              getInvoiceNumber={props.invoiceNumber}
              setIsCheck={setIsCheck} 
              placeholder="Masukkan nomor invoice" />
            { errors.invoiceNumber && touched.invoiceNumber ? <div className='italic text-sm text-red'>{errors.invoiceNumber}</div> : null }
            <span />
          </div>
          <div className={styles.groupField}>
            <label htmlFor="invoiceRelease">Invoice Terbit</label>
            <DateField name="invoiceRelease"  max={values.invoiceDueDate} placeholder="Pilih tanggal terbit" />
            { errors.invoiceRelease && touched.invoiceRelease ? <div className='italic text-sm text-red'>{errors.invoiceRelease}</div> : null }
            <span />
          </div>
          <div className={styles.groupField}>
            <label htmlFor="projectName">Nama Project</label>
            <ProjectName name='projectName'  placeholder='Masukkan Nama Project'/>
            { errors.projectName && touched.projectName ? <div className='italic text-sm text-red'>{errors.receiver}</div> : null }
            <span />
          </div>
          <div className={styles.groupField}>
            <label htmlFor="invoiceDueDate">Jatuh Tempo</label>
            <DateField name="invoiceDueDate" min={values.invoiceRelease}  placeholder="Pilih tanggal jatuh tempo" />
            { errors.invoiceDueDate && touched.invoiceDueDate ? <div className='italic text-sm text-red'>{errors.invoiceDueDate}</div> : null }
            <span />
          </div>
          <div className={styles.groupField}>
            <label htmlFor="receiver">Perusahaan Tertagih</label>
            <SelectionField  name='receiver' placeholder='Pilih Perusahaan' selectOptions={selectOptions} />
            { errors.receiver && touched.receiver ? <div className='italic text-sm text-red'>{errors.receiver}</div> : null }
            <span />
          </div>
          <div className={styles.textArea}>
            <label htmlFor="projectDescription">Deskripsi Project</label>
            <Description className="flex-grow" name='projectDecsription' placeholder='Masukkan Deskripsi Project'/>
            { errors.projectDecsription && touched.projectDecsription ? <div className='italic text-sm text-red'>{errors.receiver}</div> : null }
            <span />
          </div>
          <div className={styles.groupField}>
            <label htmlFor="ammount">Nominal</label>
            <IconField name="ammount" placeholder="Masukkan nominal invoice" />
            { errors.ammount && touched.ammount ? <div className='italic text-sm text-red'>{errors.ammount}</div> : null }
            <span />
          </div>
        </div>
        <div className="flex justify-end mt-4 sm:mt-7">
          <button 
            type="button" 
            className={ styles.reset } 
            onClick={() => { 
              resetForm()
              if (isCheck === true) {
                setFieldValue('invoiceNumber',values.invoiceNumber)
                props.updatePreview({...initialValues,invoiceNumber: values.invoiceNumber });
              } else {
                props.updatePreview(initialValues)
              }
            }} 
          >
            Reset
          </button>
            <button  
              type="submit" 
              className={ styles.submit } 
              disabled={ handleDisable(values,errors) }>
                { props.isLoading === true ? (
                  <svg className="mr-3 h-5 w-5 animate-spin text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"/>
                    <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"/>
                  </svg>
                ) : (
                  <p>Submit</p> 
                )}
            </button>
        </div>

      </Form>
    )}}
    </Formik>
  )
}

export default FormComponent