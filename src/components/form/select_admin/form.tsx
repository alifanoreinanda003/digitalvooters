import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import style from './style.module.css'
import SelectionField from '../../field/selectionField';
import { updateData } from '../../../api/listData_superAdmin';

export interface Props {
  setModalOpen: (val:boolean) => void
  selectedId: number 
}
const SelectAdminForm: React.FC<Props> = (props): JSX.Element => {
  const {setModalOpen,selectedId} = props
  const [errorRes, setErrorRes] = useState('')
  const initialValues = {
    adminName: '',
  };
  
  const handleSubmit = async (value:{adminName: string}) => {
    try {
      await updateData(selectedId,value)
      setModalOpen(false)
    } catch (error) {
      setErrorRes('Terjadi Kesalahan Teknis')
    }
  }
  return (
  <div>
    <Formik
      initialValues={ initialValues }
      onSubmit={(value) => { 
        handleSubmit(value)
      }}
    >
    {({values}) => {                        
      const disabled = Boolean(values.adminName)
      return(
      <Form>
        <div className={ style.form }>
          <div className={ style.groupField }>
            <label htmlFor="adminName">Pilih Admin</label>
            <SelectionField 
               name='adminName'
               placeholder='Pilih kelompok data'
               selectOptions={[
                { value: 'Pascol', label:'Pascol' },
              ]}
            />
          </div>
        </div>
        {errorRes ? <p className="flex justify-center pt-3 text-sm text-red">Terjadi Kesalahan Teknis</p> : ''}
        <div className="flex mt-5 justify-between items-center space-x-2 pt-4">
          <button 
          type="submit" 
          className={ style.submit } 
          disabled={!disabled}
          >
          Submit
          </button>
        </div>
      </Form>
    )}}
    </Formik>
    </div>
  )
}

export default SelectAdminForm