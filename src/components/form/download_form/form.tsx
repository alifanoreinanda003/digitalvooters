import React, { useState } from 'react'
import { Formik, Form } from 'formik'
import style from './style.module.css'
import SelectionField from '../../field/selectionField';

export interface Props {
  setModalOpen: (val:boolean) => void
}
const DownloadForm: React.FC<Props> = (props): JSX.Element => {
  const {setModalOpen} = props
  const [errorRes, setErrorRes] = useState('')
  const initialValues = {
    dataGroup: '',
    areaName:''
  };

  const handleSubmit = async (value:{dataGroup: string, areaName: string}) => {
    try {
      const params = `dataGroup=${value.dataGroup}&areaName=${value.areaName}`
       window.location.href=`https://servers.digivoote.com/dvt/v1/super-admin/download?${params}`
      setModalOpen(false)
    } catch (error) {
      setErrorRes('Terjadi Kesalahan Teknis')
    }
  }
  return (
  <div>
    <Formik
      initialValues={ initialValues }
      onSubmit={(value) => { 
        handleSubmit(value)
      }}
    >
    {({values}) => {                        
      const disabled = Boolean(values.dataGroup)
      return(
      <Form>
        <div className={ style.form }>
          <div className={ style.groupField }>
            <label htmlFor="dataGroup">Kelompok Data</label>
            <SelectionField 
               name='dataGroup'
               placeholder='Pilih kelompok data'
               selectOptions={[
                { value: 'Kecamatan', label:'Kecamatan' },
                { value: 'Kabupaten', label:'Kabupaten' },
              ]}
            />
          </div>
        </div>
        {errorRes ? <p className="flex justify-center pt-3 text-sm text-red">Terjadi Kesalahan Teknis</p> : ''}
        <div className="flex mt-5 justify-between items-center space-x-2 pt-4">
          <button 
          type="submit" 
          className={ style.submit } 
          disabled={!disabled}
          >
          Unduh
          </button>
        </div>
      </Form>
    )}}
    </Formik>
    </div>
  )
}

export default DownloadForm