import React from 'react'
import { Formik, Form, Field } from 'formik'
import { format } from 'date-fns'
import { AddEmployeeApiParams } from '../../../tools/types'
import SelectionField from '../../field/selectionField'
import DateField from '../../field/dateField'
import PhoneField from '../../field/phoneField'
import style from './style.module.css'

export interface FormCreate {
  name: string,
  gender: string,
  email: string,
  maritalStatus: string,
  phoneNumber: string,
  role: string,
  employedAt: Date | string
} 
export interface Props {
  employeeData: FormCreate
  editEmployee: (params:AddEmployeeApiParams) => void
}

const FormComponent: React.FC<Props> = (props): JSX.Element => {
  const { employeeData } = props
  const initialValues: FormCreate = {
    ...employeeData,
    employedAt: new Date(employeeData.employedAt)
  }

const optionsGender = [
  { value:'male', label:'Pria' },
  { value:'female', label:'Wanita'},
]
const optionsMaritalStatus = [
  { value:'unmarried', label:'Belum Menikah'},
  { value:'married', label:'Sudah Menikah'},
]

const validate = (values: FormCreate) => {
  const error = {}; 
  if (!values.name) {
    Object.assign(error,{
      name : 'Nama wajib diisi'
    })
  }
  if (!values.gender) {
    Object.assign(error,{
      gender : 'Jenis kelamin wajib diisi'
    })
  }
  if (!values.email) {
    Object.assign(error,{
      email : 'Email wajib diisi'
    })
  } 
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    Object.assign(error,{
      email : 'Email tidak valid'
    })
  } 
  if (!values.phoneNumber) {
    Object.assign(error,{
      phoneNumber : 'Nomor telephone wajib diisi'
    })
  }
  if (!values.maritalStatus) {
    Object.assign(error,{
      maritalStatus : 'Status pernikahan wajib diisi'
    })
  }
  if (!values.role) {
    Object.assign(error,{
      role : 'Divisi/role wajib diisi'
    })
  }
  if (!values.employedAt) {
    Object.assign(error,{
      employeeAt : 'Masa bekerja wajib diisi'
    })
  }
  return error
}
  return (
    <Formik
      initialValues={ initialValues }
      onSubmit={(value) => {
        const formValue = {
          ...value,
          employedAt: format(new Date(value.employedAt),'yyyy/MM/dd')
        };
        props.editEmployee(formValue)
      }}
      validate={validate}
    >
    {({errors,touched,}) => {
      return(
      <Form>
        <div className={style.form}>
          <div className={style.groupField}>
            <label htmlFor="name">Nama</label>
            <Field name="name" id="name" type="text" autoComplete="off" placeholder="Masukkan Nama Pegawai" />
            {errors.name && touched.name? <div className='italic text-sm text-red'>{errors.name}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="gender">Jenis Kelamin</label>
            <SelectionField name="gender" defaultDetail={employeeData.gender} placeholder="Pilih Jenis Kelamin" selectOptions={optionsGender}/>
            {errors.gender && touched.gender ? <div className='italic text-sm text-red'>{errors.gender}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="email">Email</label>
            <Field  name="email" id="email" type="email" autoComplete="off" placeholder="Masukan alamat email"/>
            {errors.email && touched.email ? <div className='italic text-sm text-red'>{errors.email}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="phoneNumber">No.Telepon</label>
            <PhoneField name="phoneNumber" type="text" placeholder="Masukan no.telepon"/>
            {errors.phoneNumber && touched.phoneNumber ? <div className='italic text-sm text-red'>{errors.phoneNumber}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="maritalStatus">Status Kawin</label>
            <SelectionField defaultDetail={employeeData.maritalStatus} name="maritalStatus" placeholder="Masukan Status Kawin" selectOptions={optionsMaritalStatus}  />
            {errors.maritalStatus && touched.maritalStatus ? <div className='italic text-sm text-red'>{errors.maritalStatus}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="role">Divisi / Role</label>
            <Field name="role" id="role" type="text" autoComplete="off" placeholder="Masukan nama divisi/role" />
            {errors.role && touched.role ? <div className='italic text-sm text-red'>{errors.role}</div> : null }
            <span />
          </div>
          <div className={style.groupField}>
            <label htmlFor="employedAt">Masa Bekerja</label>
            <DateField name="employedAt" max={null} min={null} placeholder='Masukan masa kerja'/>
            {errors.employedAt && touched.employedAt ? <div className='italic text-sm text-red'>{errors.employedAt}</div> : null }
            <span />
          </div>
        </div>
        <div className="flex justify-end mt-4 sm:mt-0">
          <button type="reset" className={style.reset} onClick={() => {}}>Reset</button>
          <button 
          type="submit" 
          className={style.submit} 
          disabled={ Object.values(errors || "" ).toString() !== "" }
          >Submit</button>
        </div>  
      </Form>
    )}}
    </Formik>
  )
}

export default FormComponent