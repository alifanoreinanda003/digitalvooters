import React from 'react';
import { Formik, Form, Field } from 'formik'
import style from './style.module.css'

export interface FormCreate {
  name: string
} 
const FromAddComponent: React.FC = (): JSX.Element => {
  const initialValues: FormCreate = {
    name: '',
  };
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(e) => console.log(e)
      }
    >
      <Form>
        <div className={style.form}>
          <div className="flex space-x-4">
            <div className={style.groupField}>
              <label htmlFor="type">Jenis Komponen</label>
              <Field name="type" type="type" placeholder="Pilih Jenis Komponen" />
            </div>
            <div className={style.groupField}>
              <label htmlFor="name">Nama Komponen</label>
              <Field name="text" type="name" placeholder="Masukkan nama komponen" />
            </div>
          </div>
          <div className={style.groupField}>
              <label htmlFor="value">Value</label>
              <Field name="value" type="value" placeholder="Masukkan value komponen" />
            </div>
        </div>
      </Form>
    </Formik>
  )
}

export default FromAddComponent