import React from 'react';
import { Formik, Form } from 'formik'
// import { EditAttendanceApiParams } from '../../../tools/types';
import SelectionField from '../../field/selectionField';
import style from './style.module.css'
import { editConfidence } from '../../../api/detailData';

export interface FormCreate {
  Canvasing: number
  Kedatangan: number
  Penyebaran: number
} 
export interface Props {
  selectedNIK: string 
  setModalOpen: (val:boolean) => void
}

const FromAddComponent: React.FC<Props> = (props): JSX.Element => {
  const initialValues: FormCreate = {
    Canvasing : 0,
    Kedatangan : 0,
    Penyebaran : 0,
  };

  const canvasPoint = [
    { value:1, label:'1 Point' },
    { value:2, label:'2 Point'},
    { value:3, label:'3 Point'},
  ]
  const KedatanganPoint = [
    { value:1, label:'1 Point' },
    { value:2, label:'2 Point'},
  ]
  const PersebaranAPKPoint = [
    { value:1, label:'1 Point' },
    { value:2, label:'2 Point'},
  ]
  return (
    <Formik
      initialValues={ initialValues }
      onSubmit={(values) => { 
        const confidenceFiltered = {
          canvasing: values.Canvasing,
          calon: values.Kedatangan,
          apk: values.Penyebaran,
        }
        editConfidence(props.selectedNIK, confidenceFiltered)
        props.setModalOpen(false)
        // window.location.reload();
      }}
    >
    {({ values }) => {       
      const TotalAmount = (values.Canvasing + values.Kedatangan + values.Penyebaran) * 10;
      
      return (
      <Form>
        <div className={style.form}>
            <div className={style.groupField}>
              <label htmlFor="Canvasing">Canvashing</label>
              <div className='w-28'>
                <SelectionField name='Canvasing' placeholder="Masukan Point" selectOptions={canvasPoint} /> 
              </div>
            </div>
            <div className={style.groupField}>
              <label htmlFor="Kedatangan">Kedatangan Calon</label>
              <div className='w-28'>
                <SelectionField name='Kedatangan' placeholder="Masukan Point" selectOptions={KedatanganPoint} /> 
              </div>
            </div>
            <div className={style.groupField}>
              <label htmlFor="Penyebaran">Penyebaran APK</label>
              <div className='w-28'>
                <SelectionField name='Penyebaran' placeholder="Masukan Point" selectOptions={PersebaranAPKPoint} /> 
              </div>
            </div>
            <div className={style.groupField}>
              <label htmlFor="Total">Total</label>
              <div className='w-28 text-[#091D60] font-semibold'>
                {TotalAmount}%
              </div>
            </div>
          <button 
            className="w-full px-4 py-3 font-medium rounded-lg bg-[#1877F2] text-white"
            type='submit'
          >
            Kirim
          </button>
        </div>
      </Form>
      )}}
    </Formik>
  )
}

export default FromAddComponent