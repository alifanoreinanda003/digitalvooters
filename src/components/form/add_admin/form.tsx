import React, { useState } from 'react'
import { Formik, Form, Field } from 'formik'
import { useDispatch } from 'react-redux';
import { AddAdminParams } from '../../../tools/types';
import { addAdmin } from '../../../api/listAdmin_superAdmin';
import { fetchListDataAdmin } from '../../../slices/dataAdmin_superAdmin/listData';
import style from './style.module.css'

export interface Props {
  setModalOpen: (val:boolean) => void
}
const FormComponent: React.FC<Props> = (props): JSX.Element => {
  const {setModalOpen} = props
  const dispatch = useDispatch()
  const initialValues = {
    name: '',
    username: '',
    password:'',
    phone:'',
    usersCategory:'admin',
    kodeDesa: undefined
  };
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  }
  const [errorRes, setErrorRes] = useState('')

  const handleSubmit = async (formValue:AddAdminParams) => {
    try {
      await addAdmin(formValue) && fetchListDataAdmin(tabelParams)
      setModalOpen(false)
      dispatch(fetchListDataAdmin(tabelParams))
    } catch (error) {
      setErrorRes('Terjadi Kesalahan Teknis')
    }
  }
  return (
  <div>
    <Formik
      initialValues={ initialValues }
      onSubmit={(value) => { 
        handleSubmit(value)
      }}
    >
    {({values}) => {                        
      const disabled = Boolean(
        values.name && 
        values.username &&
        values.phone && 
        values.password
        )
      return(
      <Form>
        <div className={ style.form }>
          <div className={ style.groupField }>
            <label htmlFor="nama">Nama</label>
            <Field  name="name" id="nama" type="text" autoComplete="off" placeholder="Masukan Nama" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="username">Username</label>
            <Field  name="username" id="username" type="text" autoComplete="off" placeholder="Masukan Username " />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="password">Password</label>
            <Field  name="password" id="password" type="text" autoComplete="off" placeholder="Masukan Password" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="phone">Telepon</label>
            <Field  name="phone" id="phone" type="text" autoComplete="off" placeholder="Masukan Telephone" />
            <span />
          </div>
          <div className={ style.groupField }>
            <label htmlFor="kodeDesa">Kode Wilayah</label>
            <Field  name="kodeDesa" id="kodeDesa" type="number" autoComplete="off" placeholder="Masukan Kode Wilayah" />
            <span />
          </div>
        </div>
        {errorRes ? <p className="flex justify-center pt-3 text-sm text-red">Terjadi Kesalahan Teknis</p> : ''}
        <div className="flex justify-between items-center space-x-2 pt-4">
          <button 
          type="submit" 
          className={ style.submit } 
          disabled={!disabled}
          >
          Submit
          </button>
        </div>
      </Form>
    )}}
    </Formik>
    </div>
  )
}

export default FormComponent