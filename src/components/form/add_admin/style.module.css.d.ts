declare const styles: {
  readonly "form": string;
  readonly "groupField": string;
  readonly "reset": string;
  readonly "submit": string;
};
export = styles;

