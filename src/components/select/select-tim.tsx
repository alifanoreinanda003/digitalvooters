import React, { useEffect, useState } from 'react'
import Select from 'react-select'
import { getTimList } from '../../api/getListTim';

export interface Props {
  onChange: (value:string | undefined) => void
}


const SelectTim: React.FC<Props> = (props): JSX.Element => {
  const { onChange } = props
  const [options, setOptions] = useState()
  
  const fetchListTim = async () => {
    try {
      const response = await getTimList()
      const mapOptions = response && response?.data?.map((item) => ({
        value:item.id, label:item.name
      }))
      setOptions(mapOptions)
    } catch (error) {
      //
    }
  }

  
 
  useEffect(() => {
    fetchListTim()
  },[])

  return(
    <div>
        <Select 
            isClearable
            id="company"
            className='text-black capitalize'
            options={options}
            placeholder="Pilih nama Tim"
            components={{ IndicatorSeparator:() => null }}
            onChange={(val) => onChange(val?.value || '')}
        />
    </div>
  )
}
export default SelectTim