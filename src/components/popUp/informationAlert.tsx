import React from 'react';
import Modal from 'react-modal';
import IcClose from '../../assets/icons/ic-cloce.svg'
import IcSuccess from '../../assets/images/ic-success.png'
import IcWarning from '../../assets/images/ic-warning.png'



export interface Props {
 isOpen: boolean
 message: string
 isSuccess:boolean
 setOpenModal: (value:boolean) => void
}

const InformationAlert: React.FC<Props> = (props): JSX.Element => {
  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.50)'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline:'none',
      border:'none',
      borderRadius: '10px',
    },
  };
  return (
    <Modal ariaHideApp={ false } isOpen={ props.isOpen } style={ customStyle }>
      <div className="w-80 text-center">
        <div className='flex justify-end'>
          <input type="image" src={ IcClose } alt='ic-close'
            onClick={() => { 
            props.setOpenModal(false)
          }}/>
        </div>
        <div className='flex justify-center items-center' >
          { props.isSuccess === true && <img className="w-32" src={ IcSuccess } alt='icon-success' /> } 
          { props.isSuccess === false && <img className="w-32" src={ IcWarning } alt='icon-warning' /> }
        </div>
        <div className="mt-7 font-semibold text-2xl text-[#091D60] ">{ props.message }</div>
      </div>
        <button className="w-full pr-0 py-3 mt-4 font-medium rounded-lg bg-[#1877F2] text-white"  
        onClick={() => {
          props.setOpenModal(false)
        }}>
        Oke
      </button>
    </Modal>
  )
}
export default InformationAlert