import React from 'react';
import Modal from 'react-modal';
import IcClose from '../../assets/icons/close.svg'
import IcDeleteUser from '../../assets/icons/ic-delete-user.png'

export interface Props {
  selectedId : number
  selectedName : string
  modalOpen: boolean
  errorRes: string
  funcDelete: (id:number) => void
  setModalOpen: (val:boolean) => void
  setErrorRes: (val:string) => void
}

const DeletePopUp: React.FC<Props> = (props): JSX.Element => {
  const {funcDelete,modalOpen,setModalOpen,selectedId,selectedName,errorRes,setErrorRes } = props
 
  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex:'10',
      overflow:'visible',
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline:'none',
      border:'none',
      borderRadius: '10px',
      paddingBottom: '25px'
    },
  };
  return (
    <Modal ariaHideApp={false} isOpen={modalOpen} style={customStyle}>
      <div className="w-80 h-72 text-center">
        <div  className="flex justify-end h-fit">
          <input type="image" src={IcClose} alt='ic-close'  
            onClick={() => {
              setErrorRes('')
              setModalOpen(false)
            }}
          />
        </div>
        <div className="grid justify-items-center text-white space-y-4">
          <h1 className="font-semibold text-2xl text-[#091D60]">Hapus Data</h1>
          <div className='grid place-items-center pt-3 space-y-2'>
            <img className="animate-pulse" src={IcDeleteUser} alt='ic-delete-user' />
            <p className="font-semibold text-xl text-[#6D6D6D]">{selectedName}</p>
          </div>
        </div>
        {errorRes ? <p className="text-sm text-red">Failed to delete data</p> : ''}
        <div className="mt-7 space-x-3">
          <button className='w-28 h-9 bg-secondary-1 text-white rounded-10' 
            onClick={() => {
              setErrorRes('')
              setModalOpen(false)
            }}
          
          >Batal</button>
          <button className='w-28 h-9 bg-red text-white rounded-10' 
          onClick={() => {
              funcDelete(selectedId)
            }}>
              Hapus
          </button>
        </div>
      </div>
    </Modal>
  )
}
export default DeletePopUp