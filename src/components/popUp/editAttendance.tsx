import React from "react";
import Modal from 'react-modal';
import Form from "../form/attendance_edit/form"
import IcClose from '../../assets/icons/close.svg'

export interface Props {
  modalOpen: boolean
  setModalOpen: (val:boolean) => void
  selectedNIK: string,
  selectedName: string,
}

const EditAttendanceModal: React.FC<Props> = (props):JSX.Element => {
  const { modalOpen, setModalOpen, selectedNIK, selectedName} = props
  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex:'10',
      overflow:'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline:'none',
      border:'none',
      borderRadius: '10px',
    },
  };

  return (
    <Modal isOpen={ modalOpen } style={ customStyle } ariaHideApp={false}>
      <div className='w-80'>
        <div className="flex justify-end">
           <input type="image" src={ IcClose } alt='ic-close' onClick={() => setModalOpen(false) } />
        </div>
        <div className="flex justify-center text-center">
          <div>
            <h1 className="font-semibold text-xl text-[#091D60]">{selectedName}</h1>
            <p className="font-semibold text-base text-[#6D6D6D]">{selectedNIK}</p>
          </div>
        </div>
        <Form 
          selectedNIK={selectedNIK}
          setModalOpen={ setModalOpen }
        />
      </div>
    </Modal>
  )
}
export { EditAttendanceModal }