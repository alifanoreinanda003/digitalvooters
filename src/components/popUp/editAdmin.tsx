import React from "react";
import Modal from 'react-modal';
import Form from "../form/edit_Admin/form"
import IcClose from '../../assets/icons/close.svg'
// import { EditAttendanceApiParams } from '../../tools/types';


export interface Props {
  modalOpen: boolean
  setModalOpen: (val:boolean) => void
  initialData: object
}

const EditAdminModal: React.FC<Props> = (props):JSX.Element => {
  const { modalOpen, setModalOpen, initialData} = props  
  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex:'10',
      overflow:'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline:'none',
      border:'none',
      borderRadius: '10px',
    },
  };

  return (
    <Modal isOpen={ modalOpen } style={ customStyle } ariaHideApp={false}>
      <div className='w-80'>
        <div className="flex justify-end">
           <input type="image" src={ IcClose } alt='ic-close' onClick={() => setModalOpen(false) } />
        </div>
        <div className="flex justify-center text-center">
          <div>
            <h1 className="font-semibold text-xl text-[#091D60]">Edit Admin</h1>
          </div>
        </div>
        <Form 
          initialData={initialData}
          setModalOpen={ setModalOpen }
        />
      </div>
    </Modal>
  )
}
export { EditAdminModal }