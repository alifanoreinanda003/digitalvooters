import React from "react";
import Modal from 'react-modal';
import IcClose from '../../assets/icons/close.svg'
import IcUser from '../../assets/images/user.png'
import SelectAdminForm from "../form/select_admin/form";


export interface Props {
  modalOpen: boolean
  setModalOpen: (val:boolean) => void
  selectedId: number
  selectedName: string
}

const SelectAdmin: React.FC<Props> = (props):JSX.Element => {
  const { modalOpen, setModalOpen, selectedId,selectedName } = props  
  
  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex:'10',
      overflow:'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline:'none',
      border:'none',
      borderRadius: '10px',
    },
  };

  return (
    <Modal isOpen={ modalOpen } style={ customStyle } ariaHideApp={false}>
      <div className='w-80'>
        <div className="flex justify-end">
           <input type="image" src={ IcClose } alt='ic-close' onClick={() => setModalOpen(false) } />
        </div>
        <div className="flex justify-center text-center mb-6">
          <div>
            <h1 className="font-semibold text-xl text-[#091D60]">Validasi Data Ganda</h1>
          </div>
        </div>
        <div className="grid place-items-center mb-4 font-semibold text-sm text-[#091D60]">
          <img className="w-28 mb-2" src={IcUser} alt="icon"/>
          <p>{selectedName}</p>
          <p>{selectedId}</p>
        </div>
        <SelectAdminForm selectedId={selectedId} setModalOpen={setModalOpen}/>
      </div>
    </Modal>
  )
}
export { SelectAdmin }