import React from 'react'
import Modal from 'react-modal'
import IcClose from '../../assets/icons/ic-cloce.svg'
import IcWhatsApp from '../../assets/icons/ic-whatsapp.png'
import IcEmail from '../../assets/icons/ic-email.png'

export interface Props {
  modalOpen: boolean
  setModalOpen: (val: boolean) => void
  data: string
}

const SharePopUp: React.FC<Props> = (props): JSX.Element => {
  const {modalOpen, setModalOpen, data} = props

  const customStyle = {
    overlay: {
      backgroundColor: 'rgba(30, 30, 30, 0.50)',
      marginRight: 'auto',
      marginLeft: 'auto',

      'z-index': '20',
    },
    content: {
      marginRight: 'auto',
      marginLeft: 'auto',
      width: '60%',
      borderRadius: '10px',
    },
  }
  return (
    <Modal ariaHideApp={false} isOpen={modalOpen} style={customStyle}>
      <div className="text-center h-full z-50 mx-auto relative pt-8 flex flex-col">
        <iframe src={data} height="100%" width="100%" />
        <div className="flex justify-end absolute -right-3 -top-3">
          <img
            src={IcClose}
            className="invert cursor-pointer w-8"
            alt="ic-close"
            onClick={() => {
              setModalOpen(false)
            }}
            onKeyDown={() => setModalOpen(false)}
            role="presentation"
          />
        </div>
        <div className="flex mt-8 justify-end items-center flex-shrink-0">
          <div className="text-secondary-1 text-lg font-semibold">Bagikan:</div>
          <div className="flex justify-between items-center text-center space-x-4">
            <div className="grid text-base text-center text-white ml-2">
              <img
                className="h-8 cursor-pointer"
                role="presentation"
                src={IcWhatsApp}
                alt="ic-whastapp"
              />
            </div>
            <div className="text-center text-base text-white">
              <img
                className="h-8 cursor-pointer"
                role="presentation"
                src={IcEmail}
                alt="ic-email"
              />
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}
export default SharePopUp
