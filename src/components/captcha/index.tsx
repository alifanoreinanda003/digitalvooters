import React, { useEffect } from 'react'

interface Captchaprops {
  setCaptchaCode: (value: String) => void
}

const Index: React.FC<Captchaprops> = ({ setCaptchaCode }): JSX.Element => {

  function loadCaptchaEnginge () {
    const showNum = [];
    const canvasWinth = 150;
    const canvasHeight = 30;
    let canvas = null;
    canvas = document.getElementById('canvas') as
        | HTMLCanvasElement
        | null
        | undefined;
        
    let rightCode = '';
    const context = canvas?.getContext('2d');
    if(context){
      const sCode = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b ,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9';
      const saCode = sCode.split(',');
      const saCodeLen = saCode.length;
      for (let i = 0; i <= 5; i++) {
        const sIndex = Math.floor(Math.random()*saCodeLen);
        const sDeg = (Math.random()*30*Math.PI) / 180;
        const cTxt = saCode[sIndex];
        showNum[i] = cTxt;
        const x = 45 + i*20;
        const y = 20 + Math.random()*8;
        context.font = 'bold 23px 微软雅黑';
        context.translate(x, y);
        context.rotate(sDeg);
    
        context.fillStyle = randomColor();
        context.fillText(cTxt, 0, 0);
    
        context.rotate(-sDeg);
        context.translate(-x, -y);
      }
      for (let i = 0; i <= 5; i++) {
        context.strokeStyle = randomColor();
        context.beginPath();
        context.moveTo(
          Math.random() * canvasWinth,
          Math.random() * canvasHeight
        );
        context.lineTo(
          Math.random() * canvasWinth,
          Math.random() * canvasHeight
        );
        context.stroke();
      }
      for (let i = 0; i < 30; i++) {
        context.strokeStyle = randomColor();
        context.beginPath();
        const x = Math.random() * canvasWinth;
        const y = Math.random() * canvasHeight;
        context.moveTo(x,y);
        context.lineTo(x+1, y+1);
        context.stroke();
      }
      
    }
    rightCode = showNum.join('');
    setCaptchaCode(rightCode);
    
  }
  
  function randomColor () {
    const r = Math.floor(Math.random()*256);
    const g = Math.floor(Math.random()*256);
    const b = Math.floor(Math.random()*256);
    return `rgb(${  r  },${  g  },${  b  })`;
  }

  useEffect(() => {
    loadCaptchaEnginge()
  }, [])

  return (
    <canvas id="canvas" width="200" height="40" className="bg-white" />
  )
}

export default Index;