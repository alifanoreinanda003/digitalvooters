import React, { useCallback } from 'react';
import DataTable from 'react-data-table-component'
import type {TableStyles, TableColumn} from 'react-data-table-component'
import { ListAdminSuperAdmin, MetaPagination } from '../../tools/types';
import { CellSkeleton } from './skeleton'
import { EditAdminModal } from '../../components/popUp/editAdmin';
import Pencil from '../../assets/icons/edit.svg'
import Delete from '../../assets/icons/ic-delete.svg'
import DeletePopUp from '../../components/popUp/deleteAlert';
import { deleteAdmin } from '../../api/listAdmin_superAdmin';
import { useAppDispatch } from '../../redux/hooks';
import { fetchListDataAdmin } from '../../slices/dataAdmin_superAdmin/listData';

interface Props {
  data: Array<ListAdminSuperAdmin>
  loading?: boolean
  meta: MetaPagination
  paginationPerPage?: number
  onChangePage: (page: number) => void,

}

const defaultProps = {
  loading: false,
  paginationPerPage: 10
}

const TableComponent: React.FC<Props> = ({
  data, loading, paginationPerPage ,meta ,onChangePage
}): JSX.Element => {
  const loadingCell = useCallback((cell) => {
    return loading
      ? <CellSkeleton>{cell}</CellSkeleton>
      : cell
  }, [loading])
  const dispatch = useAppDispatch()
  const [ modalEdit, setModalEdit ] = React.useState(false);
  const [ modalDelete, setModalDelete ]= React.useState(false);
  const [ initialData, setInitialdata] = React.useState({})
  const [ errorRes , setErrorRes ] = React.useState('')
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  }
  const handleDelete = async (id:number) => {
    try {
      const response = await deleteAdmin(id)
      if (!response.success){
        setModalDelete(true)
        setErrorRes(response.message)
      } else {
        setModalDelete(false)
        dispatch(fetchListDataAdmin(tabelParams))
      }      
    } catch (error) {
      // setErrorRes(error as any)
    }
  }
  const columns: Array<TableColumn<ListAdminSuperAdmin>> = [
    {
        name: 'No.',
        cell: (row) => loadingCell(row.no),
        width:'5rem'
    },
    {
        name: 'Nama',
        cell: (row) => loadingCell(row.name),
    },
    {
        name: 'Telephone',
        cell: (row) => loadingCell(row.phone),
    },
    {
      name: 'Kategori Admin',
      cell: (row) => loadingCell(row.category),
    },
    {
      name:'Aksi',
      cell: (row) => loadingCell(
        <div className='flex space-x-1'>
          <div className="cursor-pointer" role='presentation'  onClick={() => {
            setInitialdata(row)
            setModalEdit(true)
          }} >
            <img src={Pencil} alt='eye'/>
          </div>
          <div className="cursor-pointer" role='presentation'  onClick={() => {
              setInitialdata(row)
              setModalDelete(true)
          }} >
            <img src={Delete} alt='eye'/>
          </div>
        </div>
      ),
      width:'5rem'
    }
  ]
  const customStyles: TableStyles = {
    table: {
      style: {
        backgroundColor: 'none',
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);',
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  }

  return (
    <div>
      <DataTable
        columns={columns}
        data={data}
        customStyles={customStyles}
        pagination
        paginationServer
        paginationPerPage={paginationPerPage}
        paginationDefaultPage={meta?.page}
        paginationTotalRows={meta?.totalData}
        onChangePage={onChangePage}
        paginationComponentOptions={{
          noRowsPerPage: true,
          rangeSeparatorText: 'dari'
        }}
      />
      <EditAdminModal 
        initialData={ initialData }
        modalOpen={ modalEdit } 
        setModalOpen={ setModalEdit }
      />
      <DeletePopUp 
        modalOpen={ modalDelete }
        setModalOpen={ setModalDelete }
        selectedName={ initialData.name}
        selectedId={ initialData.id }
        funcDelete={handleDelete}
        errorRes={errorRes}
        setErrorRes={setErrorRes}
      />
    </div>
  )
}

TableComponent.defaultProps = defaultProps

export default TableComponent