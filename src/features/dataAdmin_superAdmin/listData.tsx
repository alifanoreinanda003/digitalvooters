import React,{ useEffect, useState } from "react";
import Table from './table'
import { AddAdminModal } from "../../components/popUp/addAdmin";
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchListDataAdmin, setLoadMore } from "../../slices/dataAdmin_superAdmin/listData";
import Search from '../../assets/icons/ic-search.svg'
import AddUser from '../../assets/icons/ic-addUser.svg'


const ListData: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data, meta } = useAppSelector(state => state.listAdminSuperAdmin)
  const [ searchValue, setSearchValue ] = useState('')
  const [addAdminModal , setAddAdminModal] = useState(false)
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: searchValue
  }
  const onChangePage = (page: number): void => {
    dispatch(setLoadMore(true))
    dispatch(fetchListDataAdmin(
      {
      page,
      size: tabelParams.size,
      order: '',
      sort: '',
      search: searchValue
    }))
  }
  useEffect(() => {
    dispatch(fetchListDataAdmin(tabelParams))
  },[searchValue])
  return(
    <div className="bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2">
      <div className="flex justify-between items-center">
        <h1 className="text-xl text-[#091D60] font-semibold">
          Data Admin
        </h1>
        <div className="flex space-x-3">
          <div className="flex justify-center items-center p-2 bg-[#DEE9FF] rounded-lg space-x-3 ">
            <img src={Search} alt="search" />
            <input
              className="outline-none text-black bg-transparent"
              placeholder="Cari Nama"
              onChange={(e) => setSearchValue(e.target.value)}
            />
          </div>
          <div 
            className="flex justify-center items-center p-2 bg-[#0957F9] rounded-lg space-x-3 cursor-pointer"
            role='presentation'
            onClick={() => setAddAdminModal(true)}
          >
            <img src={AddUser} alt="search" />
            <h1>Tambah</h1>
          </div>
        </div>
      </div>
      <Table
        data={data}
        loading={false}
        meta={meta}
        onChangePage={onChangePage}
        paginationPerPage={tabelParams.size}
      /> 
      <AddAdminModal 
        modalOpen = {addAdminModal}
        setModalOpen={setAddAdminModal}
      />
    </div>
    
  )
}
export { ListData }