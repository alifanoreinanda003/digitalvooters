import React,{ useEffect, useState } from "react";
import Table from './table'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchListDataTabel, setLoadMore } from "../../slices/beranda_superAdmin/listData";
import { UploadModal } from "../../components/popUp/uploadFile";
import IcSearch from '../../assets/icons/ic-search.svg'
import IcUpload from '../../assets/icons/fi-sr-cloud-upload.svg'
import IcDownload from '../../assets/icons/fi-sr-cloud-download.svg'
import { DownloadModal } from "../../components/popUp/downloadModal";

const ListData: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data, meta } = useAppSelector(state => state.listDataSuperAdmin)
  const [downloadPopUp, setDownloadPopUp] = useState(false)
  const [searchValue , setSearchValue] = useState('')
  const [ uploadModal, setUploadModal ] = useState(false)
  
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: searchValue
  }
  const onChangePage = (page: number): void => {
    dispatch(setLoadMore(true))
    dispatch(fetchListDataTabel(
      {
      page,
      size: tabelParams.size,
      order:'',
      sort:searchValue,
      search:''
    }))
  }
 
  useEffect(() => {
    dispatch(fetchListDataTabel(tabelParams))
  },[searchValue])

  return(
    <div className="bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2">
      <div className="flex justify-between items-center">
        <h1 className="text-xl text-[#091D60] font-semibold">
          Tabel Data
        </h1>
        <div className="flex space-x-3">
          <div className="flex justify-center items-center p-2 bg-[#DEE9FF] rounded-lg space-x-3 ">
            <img src={IcSearch} alt="search" />
            <input
              className="outline-none text-black bg-transparent"
              placeholder="Cari Nama"
              onChange={(e) => setSearchValue(e.target.value)}
            />
          </div>
          <div 
            className="flex justify-center items-center p-2 bg-[#0957F9] rounded-lg space-x-3 cursor-pointer"
            role='presentation'
            onClick={() =>  setUploadModal(true)}
          >
            <img src={IcUpload} alt="upload" />
            <h1>Upload File</h1>
          </div>
          <div 
            className="flex justify-center items-center p-2 bg-[#0957F9] rounded-lg space-x-3 cursor-pointer"
            role='presentation'
            onClick={() => setDownloadPopUp(true)}
          >
            <img src={IcDownload} alt="download" />
            <h1>Unduh File</h1>
          </div>
        </div>
      </div>
      <Table
        data={data}
        loading={false}
        meta={meta}
        onChangePage={onChangePage}
        paginationPerPage={tabelParams.size}
      />
      <UploadModal
        modalOpen={uploadModal} 
        setModalOpen={setUploadModal}   
      /> 
      <DownloadModal 
        modalOpen={downloadPopUp}
        setModalOpen={setDownloadPopUp}
      />
    </div>
    
  )
}
export { ListData }