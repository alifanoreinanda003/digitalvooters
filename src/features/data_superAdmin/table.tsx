import React, { useCallback, useState } from 'react';
import DataTable from 'react-data-table-component'
import type {TableStyles, TableColumn} from 'react-data-table-component'
import { useDispatch } from 'react-redux';
import { ListDataTabelSuperAdmin, MetaPagination } from '../../tools/types';
import { CellSkeleton } from './skeleton'
import { EditAttendanceModal } from '../../components/popUp/editAttendance';
import DeletePopUp from '../../components/popUp/deleteAlert';
import Delete from '../../assets/icons/ic-delete.svg'
import { deleteData } from '../../api/listData_superAdmin';
import { fetchListDataTabel } from '../../slices/beranda_superAdmin/listData';
import { AppDispatch } from '../../redux/store';
import { SelectAdmin } from '../../components/popUp/selectAdmin';

interface Props {
  data: Array<ListDataTabelSuperAdmin>
  loading?: boolean
  meta: MetaPagination
  paginationPerPage?: number
  onChangePage: (page: number) => void,

}
const defaultProps = {
  loading: false,
  paginationPerPage: 10
}

const TableComponent: React.FC<Props> = ({
  data, loading, paginationPerPage ,meta ,onChangePage
}): JSX.Element => {
  const dispatch = useDispatch<AppDispatch>()
  const [ modalOpen, setModalOpen ] = React.useState(false);
  const [ modalDelete, setModalDelete ] = React.useState(false);
  const [ selectedNIK, setSelectedNIK ] = useState('')
  const [selectedId, setSelectedId] = useState(0)
  const [ selectedName, setSelectedName ] = useState('')
  const [selectAdmin, setSelectAdmin] = useState(false)
  const [errorRes, setErrorRes] = useState('')
  const loadingCell = useCallback((cell) => {
    return loading
      ? <CellSkeleton>{cell}</CellSkeleton>
      : cell
  }, [loading])
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: '',
  }
  const handleDeleteData = async (id:number) => {
     try {
      const response = await deleteData(id)
      if(!response.success){
        setErrorRes(response.message)
        setModalDelete(true)
      } else {
        dispatch(fetchListDataTabel(tabelParams))
        setModalDelete(false)

      }
     } catch (error) {
      // setErrorRes(error as undefined)
     }
  }


  const columns: Array<TableColumn<ListDataTabelSuperAdmin>> = [
    {
      name: 'No.',
      cell: (row) => loadingCell(
        <p className={row.tps === 2 ?  "text-red" : ''}>
          {row.no}
        </p>
      ),
      width:'5rem'

    },
    {
        name: 'Nama',
        cell: (row) => loadingCell(
          <p className={row.tps === 2 ?  "text-red" : ''}>
          {row.nama}
          </p>
        ),
    },
    {
        name: 'NIK',
        cell: (row) => loadingCell(
          <p className={row.tps === 2 ?  "text-red" : ''}>
            {row.nik}
          </p>
        ),
    },
    {
      name: 'Desa',
      cell: (row) => loadingCell(
        <p className={row.tps === 2 ?  "text-red" : ''}>
          {row.desa}
        </p>
      ),
    },
    {
      name: 'Kode Desa',
      cell: (row) => loadingCell(
        <p className={row.tps === 2 ?  "text-red" : ''}>
          {row.kodeDesa}
        </p>
      ),
    },
    {
      name: 'TPS',
      cell: (row) => loadingCell(
        <p className={row.tps === 2 ?  "text-red" : ''}>
          {row.tps}
        </p>
      ),
    },
    {
      name: 'Status Data',
      cell: (row) => loadingCell(
        <button 
          className={`w-fit px-3 text-center ${row.ganda ?  "text-white bg-red" : ''} rounded-10`}
          disabled={Boolean(!row.ganda)}
          onClick={() => {
            setSelectAdmin(true)
            setSelectedId(row.id)   
            setSelectedName(row.name)
          }}
        >
          {row.ganda ? 'Ganda' : 'Valid'}
        </button>
      ),
    },
    {
      name:'Aksi',
      cell: (row) => loadingCell(
        <div className='flex space-x-1'>
          <div className="cursor-pointer" role='presentation'  onClick={() => {
            setModalDelete(true)
            setSelectedId(row.id)   
            setSelectedName(row.name)
            setSelectedNIK(row.nik)
          }} >
            <img src={Delete} alt='eye'/>
          </div>
        </div>
      ),
      width:'5rem'
    }
  ]
  const customStyles: TableStyles = {
    table: {
      style: {
        backgroundColor: 'none',
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);',
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  }

  return (
    <div>
      <DataTable
        columns={columns}
        data={data}
        customStyles={customStyles}
        pagination
        paginationServer
        paginationPerPage={paginationPerPage}
        paginationDefaultPage={meta?.page}
        paginationTotalRows={meta?.totalData}
        onChangePage={onChangePage}
        paginationComponentOptions={{
          noRowsPerPage: true,
          rangeSeparatorText: 'dari'
        }}
      />
      <EditAttendanceModal 
        selectedNIK={ selectedNIK }
        selectedName={ selectedName }
        modalOpen={ modalOpen } 
        setModalOpen={ setModalOpen }
      />
      <DeletePopUp 
        selectedId={ selectedId }
        selectedName={ selectedName }
        modalOpen={modalDelete}
        setModalOpen={setModalDelete}
        funcDelete={ handleDeleteData }
        errorRes={errorRes}
        setErrorRes={setErrorRes}
      />
      <SelectAdmin 
        modalOpen={selectAdmin}
        setModalOpen={setSelectAdmin}
        selectedId={ selectedId }
        selectedName={ selectedName }
      /> 
    </div>
  )
}

TableComponent.defaultProps = defaultProps

export default TableComponent