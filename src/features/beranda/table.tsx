import React, { useCallback, useState } from 'react';
import DataTable from 'react-data-table-component'
import type {TableStyles, TableColumn} from 'react-data-table-component'
import Modal from 'react-modal'
import { ListDataTim, MetaPagination } from '../../tools/types';
import { CellSkeleton } from './skeleton'
import IcClose from '../../assets/icons/ic-cloce.svg'
import IcDeleteUser from '../../assets/icons/ic-delete-user.png'

interface Props {
  data: Array<ListDataTim>
  loading?: boolean
  meta: MetaPagination
  paginationPerPage?: number
  onChangePage: (page: number) => void,
}

const defaultProps = {
  loading: false,
  paginationPerPage: 10
}

const TableComponent: React.FC<Props> = ({
  data, loading, paginationPerPage ,meta ,onChangePage
}): JSX.Element => {
  const loadingCell = useCallback((cell) => {
    return loading
      ? <CellSkeleton>{cell}</CellSkeleton>
      : cell
  }, [loading])
  const [modalOpen, setModalOpen]= useState(false)

  const columns: Array<TableColumn<ListDataTim>> = [
    {
        name: 'Nama',
        cell: (row) => loadingCell(row.name),
    },
    {
      name: 'NIK',
      cell: (row) => loadingCell(row.nik),
  },
    {
        name: 'TPS',
        cell: (row) => loadingCell(row.tps || '-'),
    },
    {
      name: 'Nama TIM',
      cell: (row) => loadingCell(row.timName || '-'),
  },
  ]
  const modalStyle = {
    overlay: {
      backgroundColor: '#ffffff'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#294159',
      outline:'none',
      border:'none',
      borderRadius: '10px',
      },
  };
  const customStyles: TableStyles = {
    table: {
      style: {
        backgroundColor: 'none',
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);',
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  }

  return (
    <div>
      <DataTable
        columns={columns}
        data={data}
        customStyles={customStyles}
        pagination
        paginationServer
        paginationPerPage={paginationPerPage}
        paginationDefaultPage={meta?.page}
        paginationTotalRows={meta?.totalData}
        onChangePage={onChangePage}
        paginationComponentOptions={{
          noRowsPerPage: true,
          rangeSeparatorText: 'dari'
        }}
      />
       <Modal isOpen={modalOpen} ariaHideApp={false} style={modalStyle}>
          <div className="w-80 h-72 text-center">
            <div  className="flex justify-end h-fit">
              <input type="image" src={IcClose} alt='ic-close'  onClick={() => {setModalOpen(false)}}/>
            </div>
            <div className="grid justify-items-center text-white space-y-4">
              <h1 className="font-semibold text-2xl">Hapus Pegawai</h1>
              <div className='pt-3 space-y-2'>
                <img className="animate-pulse" src={IcDeleteUser} alt='ic-delete-user' />
                <p className="font-semibold">User</p>
              </div>
            </div>
            <div className="mt-7 space-x-3">
              <button className='w-28 h-9 bg-secondary-1 text-white rounded-10' onClick={() => {setModalOpen(false)}}>Batal</button>
              <button className='w-28 h-9 bg-red text-white rounded-10' 
              onClick={() => {}}>Hapus</button>
            </div>
          </div>
        </Modal>
    </div>
  )
}

TableComponent.defaultProps = defaultProps

export default TableComponent