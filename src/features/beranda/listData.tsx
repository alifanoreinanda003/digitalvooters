import React,{ useEffect } from "react";
import Table from './table'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchListDataTim, setLoadMore } from "../../slices/beranda/listDataTim";


const EmployeeList: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data,meta } = useAppSelector(state => state.listDataTim)
    
  const tabelParams = {
    page: 1,
    size: 10,
    verify: true,
    order: '',
    sort: '',
    search: ''
  }
  const onChangePage = (page: number): void => {
    dispatch(setLoadMore(true))
    dispatch(fetchListDataTim(
      {
      page,
      size: tabelParams.size,
      verify: true,
      order:'',
      sort:'',
      search:'',
    }))
  }
  useEffect(() => {
    dispatch(fetchListDataTim(tabelParams))
  },[])

  return(
    <div className="bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2">
      <div className="text-xl text-[#091D60] font-semibold">Tabel Data</div>
      <Table
          data={data}
          loading={false}
          meta={meta}
          // loadMore={loadmore}
          onChangePage={onChangePage}
          paginationPerPage={tabelParams.size}
      /> 
    </div>
    
  )
}
export { EmployeeList }