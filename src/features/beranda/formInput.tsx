import React from 'react';
import Form from '../../components/form/add_data/form';

const FormInput: React.FC = (): JSX.Element => {
  return (
    <div className="md:w-[500px] h-fit bg-white mt-3 p-6 rounded-lg shadow-lg space-y-2">
      <div className="text-xl text-[#091D60] font-semibold">Form Input</div>
      <Form />
    </div>
  )
}
export {FormInput}