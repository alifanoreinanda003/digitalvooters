import React, { useEffect } from 'react';
import style from './style.module.css';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { fetchSummary } from '../../slices/beranda/summary';

const Summary: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data } = useAppSelector(state => state.summary)

  useEffect(() => {
    dispatch(fetchSummary())
  },[])
  const menu = [
    {
      id:1,
      title:'Kepercayaan Terbanyak',
      amount: data?.totalConfidenc || 0,
      icon:require('../../assets/images/ic-chart.png'),
    },
    {
      id:2,
      title:'Total Data Terferifikasi',
      amount:data?.totalVerification || 0,
      icon:require('../../assets/images/ic-data.png'),
    },
    {
      id:3,
      title:'Total Input Data',
      amount: data?.totalData || 0,
      icon:require('../../assets/images/ic-upload.png'),
    },
  ]
  return (
    <section>
      <div className={style.wrapperCard}>
        {menu.map((item) => {
          return (
            <div className={style.summaryCard} key={item.id}>
              <div className="flex justify-center items-center w-fit h-fit">
                <img src={item.icon} alt=''/>
              </div>
              <div className='grid'>
                <span className="text-2xl font-semibold text-primary-2">{item.amount}</span>
                <span className='text-base text-secondary-3'>{item.title}</span>
              </div>
            </div>
          )
        })}
      </div>
    </section>
  )
}
export {Summary}