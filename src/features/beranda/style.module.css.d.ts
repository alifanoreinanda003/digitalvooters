declare const styles: {
  readonly "header": string;
  readonly "row": string;
  readonly "summaryCard": string;
  readonly "table": string;
  readonly "wrapperCard": string;
  readonly "wrapperGrafik": string;
};
export = styles;

