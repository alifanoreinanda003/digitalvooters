import React from 'react';
import Skeleton from 'react-loading-skeleton'
import SkeletonTheme from '../../components/skeleton';
import style from './style.module.css';

interface CellProps {
  children?: React.ReactNode
}

const defaultCellProps = {
  children: ''
}

export const CellSkeleton: React.FC<CellProps> = ({ children}): JSX.Element => {
  return (
    <SkeletonTheme>
      <span className="relative">
        {children}
        <div className="absolute top-0 right-0 bottom-0 left-0">
          <Skeleton height="100%" width="100%" />
        </div>
      </span>
    </SkeletonTheme>
  )
}

CellSkeleton.defaultProps = defaultCellProps

interface TableProps {
  numbering?: boolean
  header?: boolean
  column?: number
  row?: number
}

const defaultTableProps = {
  numbering: true,
  header: true,
  column: 5,
  row: 5,
}

const TableSkeleton: React.FC<TableProps> = ({ header, numbering, column, row }): JSX.Element => {
  let gridTemplateColumns: string = ''
  const xAxis: Array<number> = []
  const yAxis: Array<number> = []
  
  if (numbering) {
    gridTemplateColumns = '5em '
    xAxis.push(-1)
  }
  
  if (column) {
    for (let i = 0; i < column; i++) {
      xAxis.push(i)
      gridTemplateColumns += '1fr '
    }
  }

  if (row) {
    for (let i = 0; i < row; i++) {
      yAxis.push(i)
    }
  }

  return (
    <SkeletonTheme>
      {header &&
        <div className={style.header} style={{gridTemplateColumns}}>
          {column &&
            xAxis.map((x) => <Skeleton height="1em" key={x} />)
          }
        </div>
      }
      {yAxis.length &&
        yAxis.map((y) => (
          <div className={style.row} style={{gridTemplateColumns}} key={y}>
            {xAxis.length &&
              xAxis.map((x) => <Skeleton height="1em" key={x} />)
            }
          </div>
        ))
      }
    </SkeletonTheme>
  )
}

TableSkeleton.defaultProps = defaultTableProps

export default TableSkeleton