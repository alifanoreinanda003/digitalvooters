import React,{ useEffect, useState } from "react";
import Table from './table'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { fetchListDataTabel, setLoadMore } from "../../slices/beranda/listData";
import Search from '../../assets/icons/ic-search.svg'
import SelectTim from "../../components/select/select-tim";


const ListData: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data, meta } = useAppSelector(state => state.listData)
  const [ searchValue, setSearchValue ] = useState('')
  const [filterTim, setFilterTim] = useState('')
  
  const tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: searchValue,
    timCode: filterTim
  }
  const onChangePage = (page: number): void => {
    dispatch(setLoadMore(true))
    dispatch(fetchListDataTabel(
      {
      page,
      size: tabelParams.size,
      order:'',
      sort:'',
      search:searchValue
    }))
  }
  useEffect(() => {
    dispatch(fetchListDataTabel(tabelParams))
  },[searchValue,filterTim])
  return(
    <div className="bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2">
      <div className="flex justify-between items-center">
        <h1 className="text-xl text-[#091D60] font-semibold">
          Data Admin
        </h1>
        <div className="flex space-x-3">
          <SelectTim onChange={setFilterTim}/>
          <div className="flex justify-center items-center p-2 bg-[#DEE9FF] rounded-lg space-x-3 ">
            <img src={Search} alt="search" />
            <input
              className="outline-none text-black bg-transparent"
              placeholder="Cari Nama"
              onChange={(e) => setSearchValue(e.target.value)}
            />
          </div>
        </div>
      </div>
      <Table
          data={data || data}
          loading={false}
          meta={meta}
          onChangePage={onChangePage}
          paginationPerPage={tabelParams.size}
      /> 
    </div>
    
  )
}
export { ListData }