import React, { useCallback, useState } from 'react';
import DataTable from 'react-data-table-component'
import type {TableStyles, TableColumn} from 'react-data-table-component'
import { ListDataTabel, MetaPagination } from '../../tools/types';
import { CellSkeleton } from './skeleton'
import { EditAttendanceModal } from '../../components/popUp/editAttendance';
import Pencil from '../../assets/icons/edit.svg'

interface Props {
  data: Array<ListDataTabel>
  loading?: boolean
  meta: MetaPagination
  paginationPerPage?: number
  onChangePage: (page: number) => void,

}

const defaultProps = {
  loading: false,
  paginationPerPage: 10
}

const TableComponent: React.FC<Props> = ({
  data, loading, paginationPerPage ,meta ,onChangePage
}): JSX.Element => {
  const loadingCell = useCallback((cell) => {
    return loading
      ? <CellSkeleton>{cell}</CellSkeleton>
      : cell
  }, [loading])
  const [ modalOpen, setModalOpen ] = React.useState(false);
  const [ selectedNIK, setSelectedNIK ] = useState('')
  const [ selectedName, setSelectedName ] = useState('')

  const columns: Array<TableColumn<ListDataTabel>> = [
    {
        name: 'Nama',
        cell: (row) => loadingCell(row.name),
    },
    {
        name: 'NIK',
        cell: (row) => loadingCell(row.nik),
    },
    {
        name: 'TPS',
        cell: (row) => loadingCell(row.tps),
    },
    {
        name: 'Keyakinan',
        cell: (row) => loadingCell(row.confidence),
    },
    {
      name: 'Verifikasi',
      cell: (row) => loadingCell(row.verifikasi ? 'Terverifikasi' : 'Belum Terverifikasi'),
  },
    {
      name:'Aksi',
      cell: (row) => loadingCell(
        <div className='flex space-x-1'>
          <div className="cursor-pointer" role='presentation'  onClick={() => {
            setSelectedName(row.name)
            setSelectedNIK(row.nik)
            setModalOpen(true)
          }} >
            <img src={Pencil} alt='eye'/>
          </div>
        </div>
      ),
      width:'5rem'
    }
  ]
  const customStyles: TableStyles = {
    table: {
      style: {
        backgroundColor: 'none',
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);',
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  }

  return (
    <div>
      <DataTable
        columns={columns}
        data={data}
        customStyles={customStyles}
        pagination
        paginationServer
        paginationPerPage={paginationPerPage}
        paginationDefaultPage={meta?.page}
        paginationTotalRows={meta?.totalData}
        onChangePage={onChangePage}
        paginationComponentOptions={{
          noRowsPerPage: true,
          rangeSeparatorText: 'dari'
        }}
      />
      <EditAttendanceModal 
        selectedNIK={ selectedNIK }
        selectedName={ selectedName }
        modalOpen={ modalOpen } 
        setModalOpen={ setModalOpen }
      />
    </div>
  )
}

TableComponent.defaultProps = defaultProps

export default TableComponent