import React, { useEffect, useState } from 'react';
import {Doughnut} from "react-chartjs-2";
import {
  Chart as ChartJS,
  ArcElement
} from 'chart.js'
import Select from "react-select";
import { selectOptions, selectStyles } from './utils';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { fetchPieChartSuperAdmin } from '../../slices/beranda_superAdmin/dataPieChart';

ChartJS.register(
  ArcElement
)

const ValidationChart: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data } = useAppSelector(state => state.pieChartSuperAdmin)
  const [month,setMonth] = useState('')
  
  useEffect(() => {
    dispatch(fetchPieChartSuperAdmin(month))
  },[month])
  const dataPie = {
    datasets: [
      {
        data: [data?.verification, data?.notVerification],
        backgroundColor: [
          'rgba(53, 162, 235, 0.5)',
          'rgba(169,169,169, 0.5)'
        ] 
      },
    ],
    labels: ['Data Valid', 'Data Invalid']
  };
  return (
    <div className="h-fit bg-white mt-3 p-6 rounded-lg shadow-lg space-y-2">
      <div className="flex justify-between items-center">
        <h1 className="text-xl text-[#091D60] font-semibold">
        Grafik Validasi
        </h1>
        <Select 
          styles={selectStyles}
          options={selectOptions} 
          onChange={(e) => setMonth(e?.value === undefined ? '' : e.value)}
        />
      </div>
      <div className='pt-7'>
        <Doughnut
          data={dataPie} 
          options={{
            responsive: true,
            plugins: {
              legend: {
                display:true,
                position: 'bottom'
              },
            },
          }} 
        />
      </div>
    </div>
  )
}
export {ValidationChart}