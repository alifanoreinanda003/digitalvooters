import React,{ useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import Select from "react-select";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js'
import { parseISO } from "date-fns";
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { selectOptions, selectStyles } from "./utils";
import { fetchLineChartSuperAdmin } from "../../slices/beranda_superAdmin/dataLineChart";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
)

const MonthlyChart: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { data: dataGrafik } = useAppSelector(state => state.lineChartSuperAdmin)
  const [month,setMonth] = useState('')
  
  const mapDataGrafik = dataGrafik?.map((item) => item.count)
  const dateDataGrafik = dataGrafik?.map((item) => parseISO(item.date))
  
  
  useEffect(() => {
    dispatch(fetchLineChartSuperAdmin(month))
  },[month])

  const data = {
    datasets: [
      {
        data: mapDataGrafik,
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
    labels : dateDataGrafik
  };
  return(
    <div className="md:w-[69.9rem] bg-white mt-3 p-6 rounded-lg shadow-lg  space-y-2">
      <div className="flex justify-between items-center">
        <h1 className="text-xl text-[#091D60] font-semibold">
        Grafik Data Bulanan
        </h1>
        <Select 
          styles={selectStyles}
          options={selectOptions} 
          onChange={(e) => setMonth(e?.value === undefined ? '' : e.value)}
        />
      </div>
      <div className="pt-7">
        <Line
          data={data}
          options={{
            scales: {
              y: {
                  beginAtZero: true,
                  ticks: {
                    precision: 0,
                  }
              },
          },
            responsive: true,
            plugins: {
              legend: {
                display:false
              },
            },
          }} 
        />
      </div>
    </div>
    
  )
}
export { MonthlyChart }