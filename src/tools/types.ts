export type Route = {
  name?: string
  path: string
  component?: React.ReactNode
  icon?: string
  children?: Route[]
  layout?: boolean
  requireAuth? : boolean
  role?: string
}
 // dvt types\
 export type Summary = {
  code: number | null
  success: boolean
  message:string
  data: {
    confidence: string,
    verification: number | null,
    totalData: number | null,
    target: number | null
  }
 }
 export type SummarySuperAdmin = {
  code: number | null
  success: boolean
  message:string
  data: {
    confidence: string,
    admin: number | null,
    verification: number | null,
    totalData: number | null
  }
 }

 export type GetListTimName = {
  id: number,
  formatName: string
 }
 export type PieChartSuperAdmin = {
  code: number | null
  success: boolean
  message:string
  data: {
    valid: number,
    invalid: number,
    totalData: number
  }
 }
 export type StatusApiParams = {
  code: number | null
  success: boolean
  message:string
  data: {
    userId: number | null,
    name: string,
    phone: string,
    userType: string
  }
}
 export type UserInfo = {
    userId: number | null,
    name: string,
    phone: string,
    userType: string
 }
 export type DataInput = {
  no: number,
  name : string,
  nik : string,
  createdAt : string,
  phone : string,
  confidence: string,
 }
 export type ListDataTabelParams = {
  page: number,
  size: number,
  order: string,
  sort: string,
  search: string,
 }
 export type ListDataTabel = {
  name: string,
  nik: string,
  phone: string,
  tps: number,
  confidence: string,
  createdAt: string,
 }
 export type ListDataTim = {
  timName: string,
  total: number | null,
  tps: number | null
 }
 export type ListDataTabelSuperAdmin = {
  no:number,
  id: number,
  nik: string,
  name: string,
  kodeDesa: number,
  desa: string,
  tps: number,
 }
 export type EditAdminParams = {
  name: string,
  phone: string,
  username: string,
  password: string,
  usersCategory: string,
  kodeDesa: number | undefined
 }
 export type AddAdminParams = {
  name: string,
  phone: string,
  username: string,
  password: string,
  usersCategory: string,
  kodeDesa: number | undefined
 }
 export type ListAdminSuperAdmin = {
  no:number,
  id: number,
  name: string,
  noHp: string,
  status: string,
  statusCode: number,
  kodeDesa: number
 }
 export type UploadFile = {
  file:File,
  provinsi: string,
  kota: string,
  kecamatan: string,
  desa: string,

 }
 export type AddDataParams = {
  name: string,
  nik: string,
  phone: string,
  timName: string,
 }
 export type UploadFileApiParams = {
  code: number | null,
  success: boolean,
  message: string,
  data: {
    file:File,
    provinsi: string,
    kota: string,
    kecamatan: string,
    desa: string,
  }
 }
 export type AddDataApiParams = {
  code: number | null,
  success: boolean,
  message: string,
  data: {
    admin:string
    name: string,
    nik: string,
    phone: string,
    timName: string,
  }
 }
 export type ConfidenceApiParams = {
  confidence : string
 }
 // end dvt types

export type ApiResponse = {
  code: number
  success: boolean
  message: string
  data: Object | [] | null 
  meta: {
    page: number
    size: number
    totalData: number
    totalDataOnPage: number
  }
}

export type ApiResponseAuth = {
  code: number 
  success: boolean
  message:string
  data: {
    userType: string
    expAccessToken: string
    accessToken: string
    token:string
  }
}

export type PayloadAuth = {
  username: string
  password: string
  userType: string
}
export type PayloadLogout = {
  username:string
}

export type UserDataRow = {
  name: string
  bankAccount: string
  rekeningNumber: string
  phoneNumber: string
  email: string
}

export type PostEmployeeApiParams = {
  code: number | null
  success: boolean
  message:string
  data: {
    uuid:string,
    no:number | null,
    name:string,
    gender:string,
    email:string,
    phoneNumber:string,
    maritalStatus:string,
    role:string,
    employedAt:string,
  }
}

export type ListEmployeeApiParams = {
  page?: number
  size?: number
}
export type ListEmployeeDataRow = {
   employeeUuid:string,
   no:number | null,
   name:string,
   gender:string,
   email:string,
   phoneNumber:string,
   maritalStatus:string,
   role:string,
   employedAt:number | null,
}
export type AddEmployeeApiParams = {
  name: string,
  gender: string,
  email: string,
  maritalStatus: string,
  phoneNumber: string,
  role: string,
  employedAt: string
}

export type PegawaiDataRow = {
  number:number,
  name:string,
  email:string,
  phone:string,
  status:string,
  role:string,
  workPeriod:string,
}
export type Employee = {
  id: number
  no?: number
  name: string
  employeeId?: string
  role: string
  email?: string
  phone?: string
  status?: string
  lengthOfWork: string
  salary?: string
  slip?: string
}
export type Component = {
  id: number
  no?: number
  name: string
  value: string
}

export type BasicResponse = {
  data: [] | {}
  error: boolean
  loading: boolean
  code: number
  message?: string
}
export type AttendanceApiParams = {
  employeeUuid?: string | undefined,
  employeeName?: string,
  page?:number
  size?:number
  totalData?: number
  totalDataOnPage?: number
  sort?: string
}
export type DetailAttendanceData = {
  attendanceId: string,
  employeeUuid: string,
  employeeName: string,
  dataNumber: number | null,
  date:string,
  attendanceStatus: string,
  employeeShift:string,
  absentEntry: string,
  absentOut: string,
  editData: boolean,
  detailData: boolean,
}

export type EditAttendanceApiParams = {
  absentStatus: string,
  absentEntryDate: string,
  absentOutDate: string,
}

export type AttendanceDataRow = {
  dataNumber:number,
  employeeName: string,
  attendanceId:string,
  absentEntry: string,
  absentOut: string,
  employeeShift: number,
  attendanceStatus: string,
}

export type PayrollApiParams = {
  skip?: number
  limit?: number
  search?: string
  orderBy?: string
  sortBy?: string
}
export type PayrollDataRow = {
  no: number
  name: string;
  employeeId: string;
  role: string;
  email: string;
  phone: string;
  status: string;
  workPeriod: string;
  salary: string;
  slip: string,
}

export type MasterCompApiParams = {
  skip?: number
  limit?: number
  search?: string
  orderBy?: string
  sortBy?: string
}
export type MasterCompDataRow = {
  number:number;
  name: string;
  value: string;
}

export type PostInvoiceApiParams = {
  code: number 
  success: boolean
  message:string
  data: {
    invoiceNumber: string
    invoiceRelease: Date | null | undefined 
    receiver: object
    invoiceDueDate: Date | null | undefined 
    ammount: number | null
  }
}

export type CreateInvoicesApiParams = {
  invoiceNumber: string
  invoiceRelease: Date | null | undefined 
  receiver: object
  invoiceDueDate: Date | null | undefined 
  ammount: number | null
}

export type InvoicesApiParams = {
  skip?: number
  limit?: number
}

export type InvoicesDataRow = {
  uuid:string;
  no: number;
  invoiceNumber: string;
  description: string;
  receiverInvoice: string;
  invoiceDate: string;
  price: string;
}
export type InvoiceNumber = {
  invoiceNumber: string,
}

export type CompanyData = {
  id: number,
  companyName:{},
  address:string,
  baankAccount:string,
}

export type MetaPagination = {
  page: number
  size: number
  totalData: number
  totalDataOnPage: number
}