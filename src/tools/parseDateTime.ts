/* eslint-disable import/no-duplicates */
import { format } from 'date-fns';
import { id } from 'date-fns/locale';

export const customFormat = (value: string | Date, pattern: string = 'PPPP'): string => {
  const date: Date = new Date(value)
  const formated: string = format(date, 'PPPP', { locale: id })
  return formated
}
