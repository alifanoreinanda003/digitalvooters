import { ApiResponse } from "./types";

const BASIC_USER = process.env.REACT_APP_BASIC_AUTH_USERNAME as string
const BASIC_PASS = process.env.REACT_APP_BASIC_AUTH_PASSWORD as string
const BasicCredentials = Buffer.from(
  `${BASIC_USER}:${BASIC_PASS}`,
  'utf-8'
).toString('base64')

export const baseFetch = (
  url: string,
  method: string,
  queryParams?: Record<string, unknown>,
  body?: Record<string, unknown>,
  headers?: Record<string, unknown>,
  ) => {
    
  const query = queryParams
    ? Object.keys(queryParams).map(key => `${key}=${queryParams[key]}`).join('&')
    : '';
  url = query ? `${url}?${query}` : url
    
  return fetch(url, {
    method,
     headers: {
      'Content-Type': 'application/json',
      'Authorization': `Basic ${BasicCredentials}`,
      ...headers
    },
    body: JSON.stringify(body),
  });
}

export const post =  async<Type> (
  endpoint: string,
  body: Record<string, unknown>,
  headers?: Record<string, unknown>,
): Promise<Type> => {
  let result = null
  const response = await baseFetch(endpoint, 'POST', undefined, body, headers)
  const data = await response.json()
  if (response.status <=400 ){
   result = Promise.resolve(data)
  } else {
    result = Promise.reject(data)
  }
  return result

}
export const get = async (
  endpoint: string,
  params?: Record<string, unknown>,
  headers?: Record<string, unknown>,
  ): Promise<ApiResponse> => {
    
  const response = await baseFetch(endpoint, 'GET', params, undefined, headers)
  const data = await response.json()
  return data
}
export const remove = async (
  endpoint: string,
  params?: Record<string, unknown>,
  headers?: Record<string, unknown>,
  ): Promise<ApiResponse> => {
  const response = await baseFetch(endpoint, 'DELETE', undefined, undefined, headers)
  const data = await response.json()
  return data
}
export const edit = async (
  endpoint: string,
  body: Record<string, unknown>,
  headers?: Record<string, unknown>,
  ): Promise<ApiResponse> => {
  const response = await baseFetch(endpoint, 'PUT', undefined, body, headers)
  const data = await response.json()
  return data
}