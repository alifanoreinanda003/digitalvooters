import {createTheme} from 'react-data-table-component'

createTheme(
  'solarized',
  {
    text: {
      primary: '#ffffff',
      secondary: '#2aa198',
    },
    divider: {
      default: 'rgba(255, 255, 255, 0.1)',
    },
  },
  'dark'
)

export const tableStyles = {
  table: {
    style: {
      fontFamily: 'Poppins, sans-serif',
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      background: 'rgba(255,255,255,0.1)',
      paddingLeft: 15,
      paddingRight: 15,
    },
  },
  head: {
    style: {
      fontFamily: 'Poppins, sans-serif',
      fontSize: '16px',
      fontWeight: 600,
    },
  },
  headRow: {
    style: {
      background: 'rgba(255,255,255,0.2)',
      borderRadius: 10,
      boredr: 'none',
    },
  },
  rows: {
    style: {
      fontFamily: 'Poppins, sans-serif',
      fontSize: 16,
      fontWeight: 600,
      background: 'transparent',
      '&:last-child': {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
      },
    },
  },
}
