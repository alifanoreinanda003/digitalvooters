import { useEffect, useState } from 'react'

export default function useScreen(): { isMobile: boolean} {
  const minWidht = 640
  const [isMobile, setIsMobile] = useState(window.innerWidth < minWidht)

  const onResize = () => {
    const { innerWidth } = window
    setIsMobile(innerWidth <= minWidht)
  }

  useEffect(() => {
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  }, [])

  return { isMobile }
}