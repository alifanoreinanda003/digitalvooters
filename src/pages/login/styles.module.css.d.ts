declare const styles: {
  readonly "container": string;
  readonly "dontHaveAccount": string;
  readonly "logo": string;
  readonly "sections": string;
  readonly "title": string;
};
export = styles;

