import React, { useState } from 'react'
import { login } from '../../api/authService'
import { PayloadAuth } from '../../tools/types'
import Form from '../../components/form/login/form'
import styles from './styles.module.css'
import DvtLogo from '../../assets/images/voting-box 1.png'

const Login: React.FC = (): JSX.Element => {
  const [ isLoading, setIsLoading ] = useState(false)
  const [ errorRes , setErrorRes ] = useState()
  
  function redirect(){
    window.location.href = "/beranda";
  }
  const authLogin = async (params: PayloadAuth):Promise<boolean> => {
    try {
      setIsLoading(true)
      const response = await login(params)
      localStorage.setItem('token_dvt',response.data.accessToken) 
      localStorage.setItem('exp_token_dvt',response.data.expAccessToken) 
      localStorage.setItem('role_dvt',response.data.userType) 
      setIsLoading(false)
      redirect()
      return true
    } catch (error) {
      setErrorRes(error  as undefined)
      setIsLoading(false)
      return false
    }
   }
    const handleSubmit = (params: PayloadAuth) => {
      authLogin(params)
    }

  return (
    <section className={styles.sections}>
      <div className="w-[400px]">
        <div className={styles.container}>
          <div className="grid justify-center pb-8">
            <div className={styles.logo}>
              <img alt="vote" src={DvtLogo} />
              <div className={styles.title}>Digital Vooters</div>
              <p className='text-black font-semibold'>Masuk untuk melanjutkan</p>
            </div>
          </div>
          <Form
            handleSubmit={ handleSubmit }
            isLoading={ isLoading }
            error= { errorRes }
          />
        </div>
      </div>
    </section>
  )
}

export {Login}
