import React from 'react';
import { Summary } from '../../features/beranda/summaryCard';
import { EmployeeList } from '../../features/beranda/listData';
import { FormInput } from '../../features/beranda/formInput';

const Beranda: React.FC = (): JSX.Element => {
  return (
    <section>
        <Summary />
        <div className="md:flex md:justify-between md:space-x-4">
        <EmployeeList />
        <FormInput/>
        </div>
    </section>
  )
}
export { Beranda }