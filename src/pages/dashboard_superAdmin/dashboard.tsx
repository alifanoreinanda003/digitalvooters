import React from 'react';
import { Summary } from '../../features/dashboard/summaryCard';
import { MonthlyChart } from '../../features/dashboard/monthlyChart';
import { ValidationChart } from '../../features/dashboard/validationChart';

const Dashboard: React.FC = (): JSX.Element => {
  return (
      <div>
        <Summary />
        <div 
          className="
            laptop:w-[47.5rem] 
            desktop:w-[63.5rem] 
            notebook:w-[69rem] 
            gaming:w-full
            md:flex md:justify-between space-x-4 z-0">
          <MonthlyChart />
          <ValidationChart/>
        </div>
      </div>
  )
}
export { Dashboard }