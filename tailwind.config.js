module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      primary: {
        '1': '#252F3E',
        '2': '#294159',
        '3': '#243A4F',
      },
      secondary: {
        '1': '#7F8D9B',
        '2': '#41E1F1',
        '3': '#9EA5AE',
        '4': '#25D366',
      },
      red: '#FF0000',
      black: '#000000',
      white: '#FFFFFF',
      base: '#F5F4F6',
    },
    extend: {
      screens: {
        'tablet': '640px',  
        'laptop': '1024px',
        'desktop': '1280px',
        'notebook': '1366px',
        'gaming': '1440px'
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
      fontSize: {
        '2xl': '26px'
      },
      borderRadius: {
        '10': '10px',
        '5': '5px',
      }
    }
  },
  plugins: [],
}
