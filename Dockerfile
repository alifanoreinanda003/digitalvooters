FROM node:16-alpine as build
WORKDIR /app
RUN apk add --update --no-cache --virtual .build-dev git
COPY package*.json ./
RUN npm i -g npm@8
RUN npm i --legacy-peer-deps
RUN apk del .build-dev
COPY . .
RUN npm run build

# nginx
FROM nginx
WORKDIR /usr/share/app
RUN rm -r /usr/share/nginx/html/*
RUN sed -i -e 's|index  index.html index.htm;|index  index.html index.htm;\n        try_files $uri /index.html;|g' \
  /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist /usr/share/nginx/html
