const gzipStatic = require("connect-gzip-static");
const historyApiFallback = require("connect-history-api-fallback");
const express = require("express");
const path = require("path");

const app = express();
const port = 4000;

app.use(gzipStatic("dist"));
app.use(historyApiFallback());
app.use(express.static(path.join(__dirname, "../dist")));

app.listen(port);

console.log(`App is running on port ${port}`);
