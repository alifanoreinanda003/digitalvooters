const common = require('../webpack.common.js')
const { merge } = require('webpack-merge')
const { TypedCssModulesPlugin } = require('typed-css-modules-webpack-plugin')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    host: '0.0.0.0',
    port: 4000,
    hot: true,
    open: {
      target: 'http://localhost:4000',
    },
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        oneOf: [
          {
            test: /\.module\.s?css$/,
            use: [
              'style-loader',
              {
                loader: 'css-loader',
                options: {
                  importLoaders: 1,
                  modules: true,
                },
              },
              'sass-loader',
              'postcss-loader',
            ],
          },
          {
            use: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader'],
          },
        ],
      },
    ],
  },
  plugins: [
    new TypedCssModulesPlugin({
      globPattern: 'src/**/*.module.@(css|scss)',
    }),
  ],
})
